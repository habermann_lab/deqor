DEQOR is a web-based tool for the design and quality control of siRNAs. 
Endoribonuclease-prepared siRNAs (esiRNAs or 'diced' RNAs) are less variable 
in their silencing capabilities and circumvent the laborious process of 
sequence selection for RNAi due to a broader range of products. Though 
powerful, this method might be more susceptible to cross-silencing genes 
other than the target itself. We have developed a web-based tool that 
facilitates the design and quality control of siRNAs for RNAi. 
The program, DEQOR, uses a scoring system based on state-of-the-art parameters 
for siRNA design to evaluate the inhibitory potency of siRNAs. DEQOR, 
therefore, can help to predict (i) regions in a gene that show high 
silencing capacity based on the base pair composition and (ii) siRNAs 
with high silencing potential for chemical synthesis. In addition, each siRNA 
arising from the input query is evaluated for possible cross-silencing 
activities by performing BLAST searches against the transcriptome or genome 
of a selected organism. 

If you use this tool, please cite: Henschel A1, Buchholz F, Habermann B. (2004) 
DEQOR: a web-based tool for the design and quality control of siRNAs. Nucleic 
Acids Res. 2004 Jul 1;32(Web Server issue):W113-20.

Last updated by Vineeth Surendranath (vineethsurendranath@gmail.com)
