from analyze import analyze

from pickle import load
from os import path, system
from glob import glob
from time import sleep

import traceback

inputfolder = "/export/deqor_v3/input"



while 1:
	try:
		discard = system("mv /tmp/deqorv2__* %s > /dev/null 2>&1" % inputfolder)
	
		input_files = glob(path.join(inputfolder, "deqorv2__*"))

		for input_file in input_files:
			id = input_file.split("__")[-1].split(".")[0]
			if path.exists(path.join(inputfolder, "%s.processed" % id)):
				continue

			parameters = load(open(input_file))
			print parameters

			discard = system("rm -f %s" % input_file)
			discard = system("touch %s" % path.join(inputfolder, "%s.processed" % id))

			analyze(id, parameters)
	except:
		traceback.print_exc()


	sleep(5)
