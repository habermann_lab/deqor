from sys import argv
from glob import glob
from os import path     
from pickle import load, dump
from deqoranalysis import findBestStretch
from primerdesign import makePrimers

deqorOutputFolder = argv[1]
outputFile = open(argv[2], "w")
errorFile = open("%s_error.txt" % "".join(argv[2].split("/")[-1].split(".")[:-1]), "w")

deqorOutputFiles = glob(path.join(deqorOutputFolder,"*.dump"))

esiRNAAnalysis = {}
forPrimers = []

for deqorFile in deqorOutputFiles:
    
    print "Processing : ", deqorFile
    
    deqorOutput = load(open(deqorFile))
    sequenceId = "".join(deqorFile.split("/")[-1].split(".")[:-1])
    
    stretches = findBestStretch(deqorOutput)
    try: bestStretch = stretches[0]
    except IndexError: 
        errorFile.write("%s\n" % sequenceId)
	continue
    
    esiRNAAnalysis[sequenceId] = bestStretch[0]
    print sequenceId, bestStretch[2]
    forPrimers.append((sequenceId, bestStretch[2]))

errorFile.close()    
primers = makePrimers(forPrimers)
for primer in primers:
    if not len(primer.strip()): continue
    outputFile.write("%s;%s\n" % (primer.strip(), esiRNAAnalysis[primer.split(";")[0]]))
outputFile.close()


    
    
    
    
    
    
    
    


