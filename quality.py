"""

An extensible framework for scoring the quality of siRNAs.

Author : Vineeth Surendranath
Date : 25th October 2007

Built-in penalties from :  
1. DEQOR: a web-based tool for the design and quality control of siRNAs, Henschel et al, NAR 2004 32(Web Server Issue)
2. A protocol for designing siRNAs with high functionality and specificity, Birmingham et al, Nature Protocols 2, 2068-2078(2007)
(this is a paper from Khvorova's lab)

"""

import re

class Quality:

	""" Each penalty exists as a callable function applied to the sequence """

	def __init__(self, sirna, **penalties):

		self.sequence = sirna.upper()
		
		self.usePolynucleotide, self.polyPenalty = True, 7
		self.useAT_GCReversed, self.reversePenalty = True, 7
		self.useAT_GCSymmetry, self.symmetryPenalty = True, 3
		self.useGCContent, self.gcPenalty = True, 1
		self.useInterferonMotif, self.interferonPenalty = False, 10
		self.useMirnaSeeds, self.mirnaPenalty = False, 10
		self.positionConstraint, self.posConstraintPenalty = True, 1

		self.positionConstraints = {19 : ["A", "T"], 3 : ["A"], 13 : ["A","T","C"], 10 : ["T"]}
		# follows from http://www.nature.com/nbt/journal/v22/n3/full/nbt936.html

		self.totalPenalties = 0

		if self.usePolynucleotide: self.totalPenalties += self.__polynucleotide(self.polyPenalty)
		if self.useAT_GCReversed: self.totalPenalties += self.__at_gcReversed(self.reversePenalty)
		if self.useAT_GCSymmetry: self.totalPenalties += self.__at_gcSymmetry(self.symmetryPenalty)
		if self.useGCContent: self.totalPenalties += self.__gcContent(self.gcPenalty)
		if self.useInterferonMotif: self.totalPenalties += self.__interferon(self.interferonPenalty)
		if self.useMirnaSeeds: self.totalPenalties += self.__mirna(self.mirnaPenalty)
		if self.positionConstraint: self.totalPenalties += self.__positionConstraint(self.posConstraintPenalty, self.positionConstraints)

		

	
	def __polynucleotide(self, penalty):
		
		polyRegex = re.compile("((?P<nucleotide>[AGTC])(?P=nucleotide){4,})") # P is used to reference a previous match
		#print "polynucleotide : ", len(polyRegex.findall(self.sequence)) * penalty
		return len(polyRegex.findall(self.sequence)) * penalty

	def __at_gcReversed(self, penalty):

		reversedRegex = re.compile("((^[GC]).+([AT]$))")
		if reversedRegex.search(self.sequence): 
			#print "reversed : ", penalty
			return penalty
		else: return 0

	def __at_gcSymmetry(self, penalty):
		
		atSymmetryRegex = re.compile("((^[AT]).+([AT]$))")
		gcSymmetryRegex = re.compile("((^[GC]).+([GC]$))")
		if atSymmetryRegex.search(self.sequence): 
			#print "symmetry : ", penalty
			return penalty
		elif gcSymmetryRegex.search(self.sequence): 
			#print "symmetry : ", penalty
			return penalty
		else: return 0

	def __gcContent(self, penalty, least = (20, 1), highest = (50, 2)):
		# penalty at per 1% for gc below 20% and per 2% for gc above 50%

		gcContent = ((self.sequence.count("G") + self.sequence.count("C"))/float(len(self.sequence))) * 100
		#print "gcContent : ", gcContent
		if gcContent < least[0]: return ((least[0] - gcContent)/least[1]) * penalty
		elif gcContent > highest[0]: return ((gcContent - highest[0])/highest[1]) * penalty
		else: return 0

	def __interferon(self, penalty):

		motif = "GTCCTTCAA"
		if self.sequence.find(motif) != -1: return penalty
		else: return 0
		
		#return reduce(lambda x, y: x + y, [penalty for motif in motifs if (self.sequence.find(motif) + 1)])

	
	def __positionConstraint(self, penalty, constraints):

		totalPosConstraintPenalty = 0
		for key in constraints.keys():
			position, nucleotides = key, constraints[key]

			if self.sequence[position] not in nucleotides: totalPosConstraintPenalty += penalty


		return totalPosConstraintPenalty
		
		
	

		
