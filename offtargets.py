from os import system
from sys import path

sirna_length = 21
bowtie = "/export/deqor_v3/bowtie/bowtie"

target_threshhold_proportion = 0.5 # what percent of the sirnas hit a target from the db, if this is >= threshhold, then this is the main target
bowtieop_columns = 8 # how many columns in the bowtie output when there is a mismatch
central_pos = 11 # for a length of 21, the central position is 11, this is for penalty calculations

perfectmatch_penalty = 10
centralmismatch_penalty = 8
noncentralmismatch_penalty = 7

def makesirnafile(sequence, sirna_file):
	
	sirna_filehandle = open(sirna_file, "w")
	for count in range(len(sequence) - sirna_length):
		sirna = sequence[count:(count + sirna_length)]
		sirna_filehandle.write(">%s\n" % count)
		sirna_filehandle.write("%s\n" % sirna)
	sirna_filehandle.close()

	return


def runbowtie(sirna_file, db_base, outputfile):
	
	bowtiecmd = ""
	bowtiecmd += bowtie
	bowtiecmd += " -f -v 1 --norc --all"
	bowtiecmd += " %s %s 1>%s" % (db_base, sirna_file, outputfile)

	discard = system(bowtiecmd)

	return

def findmaintargets(outputfile, sequence):

	num_sirnas = len(sequence) - sirna_length

	op_filehandle = open(outputfile)
	trnshash = {}
	for line in op_filehandle:
		trnsid = line.strip().split()[2]
		if trnshash.has_key(trnsid): trnshash[trnsid] += 1
		else: trnshash[trnsid] = 1
	op_filehandle.close()

        maintargets = []
        for trnsid, count in trnshash.iteritems():
                if count >= (target_threshhold_proportion * num_sirnas): maintargets.append(trnsid)
        return maintargets

def scoreofftargets(outputfile, sequence):

	maintargets = findmaintargets(outputfile, sequence)

	op_filehandle = open(outputfile)
	offtargets = {}

	for line in op_filehandle:
		parts = line.strip().split()
		num_of_parts = len(parts)

		sirna_id, target_id = parts[0], parts[2]
		if target_id in maintargets: continue

		if num_of_parts == (bowtieop_columns - 1): penalty = perfectmatch_penalty
		else:
			mismatch = parts[-1]
			mismatch_pos = int(mismatch.split(":")[0])
			if mismatch_pos == central_pos: penalty = centralmismatch_penalty
			else: penalty = noncentralmismatch_penalty
			

		if offtargets.has_key(sirna_id): offtargets[sirna_id][target_id] = penalty
		else: offtargets[sirna_id] = {target_id : penalty}

	op_filehandle.close()
	return offtargets
			
	
		
