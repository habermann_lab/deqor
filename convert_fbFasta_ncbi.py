from sys import argv
from Bio import SeqIO

fasta_file = argv[1]
fasta_handle = open(fasta_file)

ncbi_hash = {}
fasta_parser = SeqIO.parse(fasta_handle, "fasta")

for record in fasta_parser:
	fb_id = record.name
	fb_desc = record.description
	if fb_desc.find("REFSEQ") == -1: continue

	print fb_desc	
	refseq_id = fb_desc.split("REFSEQ:")[1].split(",")[0]
	ncbi_hash[refseq_id] = (fb_id, str(record.seq))

fasta_handle.close()

fasta_handle = open(fasta_file, "w")
for refseq_id in ncbi_hash.keys():
	fasta_header = ">lcl|%s|ref|%s" % (ncbi_hash[refseq_id][0], refseq_id)
	fasta_handle.write("%s\n" % fasta_header)
	fasta_handle.write(ncbi_hash[refseq_id][1])
	fasta_handle.write("\n")
fasta_handle.close()
