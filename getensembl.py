# Retrieve ensembl transcripts and sequences given their ids
from cogent.db.ensembl import Genome

def initialize_db(species, release): return Genome(Species = species, Release = release, account = None)

def get_spliceforms(ensembl_geneid, species, release, protein_coding = True):
	
	species_db = initialize_db(species, release)
	gene_record = species_db.getGeneByStableId(StableId = ensembl_geneid)
	
	transcripts = {}
	transcript_records = gene_record._get_transcripts()

	for transcript_record in transcript_records:
		if protein_coding and (not (transcript_record.BioType == "protein_coding")): continue
		transcript_id = transcript_record.StableId
		transcript_sequence = str(reduce(lambda x, y: x + y, [seq for seq in [transcript_record.Cds,transcript_record.Utr3] if seq is not None]))
		transcripts[transcript_id] = transcript_sequence

	return transcripts
