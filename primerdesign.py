from os import system, remove
primerparams = """PRIMER_PRODUCT_SIZE_RANGE=%s
PRIMER_IO_WT_GC_PERCENT_LT=0.0
PRIMER_LIB_AMBIGUITY_CONSENSUS=0
PRIMER_FIRST_BASE_INDEX=1
PRIMER_WT_GC_PERCENT_LT=0.0
PRIMER_WT_GC_PERCENT_GT=0.0
PRIMER_LIBERAL_BASE=1
PRIMER_MAX_END_STABILITY=9.0
PRIMER_EXPLAIN_FLAG=1
PRIMER_IO_WT_GC_PERCENT_GT=0.0"""

primer3Tool = "/home/vineeth/deqorht/external/primer3_core"
primerProductRange = 100

def findprimers(primerop_file):
	primerdata = []
	for line in open(primerop_file):
		if line.startswith("SEQUENCE=") or line.startswith("PRIMER_LEFT_SEQUENCE") or line.startswith("PRIMER_RIGHT_SEQUENCE"): 
			primerdata.append(line.strip().split("=")[1])
	return primerdata



def clean(id):

	"""
	Input : id of esi being dealt with
	Output : None (remove files created for primer design)
	"""


	remove("primerip_%s.txt" % id)
	#remove("primerop_%s.txt" % id)
	#remove("primerseq.txt")
	return
	

def writePrimerInputFile(sequence,id):
	
	"""
	Input : sequence for which primer is to be designed, related id
	Output : None (the input file for primer3 is written)
	"""

	primerInputFile = open("primerip_%s.txt" % id,"w")
	sequenceLength = len(sequence)
	primerInputFile.write(primerparams % ("%s-%s" % (sequenceLength - primerProductRange, sequenceLength + primerProductRange)))
	primerInputFile.write("\nPRIMER_SEQUENCE_ID=%s\n" % id)
	primerInputFile.write("SEQUENCE=%s\n" % sequence)
	primerInputFile.write("=\n")
	primerInputFile.close()

	return

def designExtractPrimers(id):

	"""
	Input : id for which the primer is to be designed, input file name relates to this id
	Output : designed primers, returned as string
	"""

	discard = system("%s < primerip_%s.txt > primerop_%s.txt" % (primer3Tool, id, id))
	

	primerLines =  findprimers("primerop_%s.txt" % id)
	clean(id)
	if len(primerLines) > 1: return "%s;%s;%s;%s;%s" % (id, primerLines[0], primerLines[1], primerLines[2], len(primerLines[0]))
	else: 
		print id
		return ""
	
def makePrimers(forPrimers):
	
	primers = []
	for entity in forPrimers: 
		id, sequence = entity[0], entity[1]
		writePrimerInputFile(sequence, id)
		primerLine = designExtractPrimers(id)
		primers.append(primerLine)
	
	return primers
	

	
