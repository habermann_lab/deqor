from sys import argv
from getensembl import get_spliceforms
from preproc import define_input
from suffixstrings import lcsString

species = "human"
release = "58"
oldnew_overlap = 0.75
minimum_inputlength = 150

def find_commonregion(ensembl_id):

	transcripts = get_spliceforms(ensembl_id, species, release)
	common_regions = define_input({ensembl_id:transcripts})

	return common_regions


def find_oldregions(primary_seq, common_regions):

	old_regions = {}
	for region_id in common_regions.keys():
		common_seq = common_regions[region_id]
		primary_len = len(primary_seq)
		
		lcs_len, lcs_seq = lcsString([primary_seq, common_seq], 0) # we are looking for an exact match of the region
		if lcs_len and (lcs_len > (oldnew_overlap * lcs_len)): old_regions[region_id] = (common_seq.find(lcs_seq), common_seq.find(lcs_seq) + lcs_len)

	return old_regions

def find_deqorinput(ensembl_id, common_regions, old_regions):

	if not len(old_regions.keys()):
		print "PrimaryEsiNotFound:",ensembl_id
		return

	input_seq = {}
	for region_id in old_regions.keys():
		
		old_start, old_stop = old_regions[region_id]
		common_seq = common_regions[region_id]

		left_seq, right_seq = common_seq[:old_start], common_seq[old_stop:]
		possibles = [(len(left_seq), left_seq), (len(right_seq), right_seq)]
		possibles.sort()

		possible_len, possible_seq = possibles[-1]
		if possible_len < minimum_inputlength:
			old_mid = (old_start + old_stop) / 2
			left_seq, right_seq = common_seq[:old_mid], common_seq[old_mid:]
			possibles = [(len(left_seq), left_seq), (len(right_seq), right_seq)]
			possibles.sort()
			possible_len, possible_seq = possibles[-1]
			
		input_seq[region_id] = possible_seq

	return input_seq

if __name__ == "__main__":
	
	input_file = open(argv[1])
	filename_base = ".".join(argv[1].split(".")[:-1])

	output_file, error_file = open(filename_base+"_deqor_input.fa", "w"), open(filename_base+"_error.txt","w")
	
	error_file.write("No primary esiRNA found in the database for these genes. Please design fresh esiRNAs for them\n")
	for line in input_file:
		ens_id, sequence = line.strip().split(",")
		print ens_id
		print "Retrieving ensembl and finding common stretches"
		curr_commonregions = find_commonregion(ens_id)
		print "Finding primary sequence"
		curr_oldregions = find_oldregions(sequence, curr_commonregions)
		print "Determining input"
		input_hash = find_deqorinput(ens_id, curr_commonregions, curr_oldregions)
		if not input_hash:
			error_file.write("%s\n" % ens_id)
			continue
		for key in input_hash.keys():
			output_file.write(">%s\n" % key)
			output_file.write("%s\n" % input_hash[key])

	input_file.close()
	error_file.close()
	output_file.close()



			
		

	 






		
		
 
	
