from sys import argv
from suffixstrings import overlapNoCulprit
from Bio import SeqIO

def make_seqhash(fastaFile):
	# this is written to parse an Ensembl cDNA file downloaded using biomart. The format of the header is >ENSG000000000|ENST0000000|...

	seqhash = {}

	records = SeqIO.parse(open(fastaFile), "fasta")
	for record in records:
		id_string = record.name
		sequence = str(record.seq)
	
		id_parts = id_string.split("|")
		gene, trns = id_parts[0], id_parts[1]

		if seqhash.has_key(gene): seqhash[gene][trns] = sequence
		else: seqhash[gene] = {trns:sequence}
	print seqhash
	return seqhash


def define_input(seqhash):

	geneids = seqhash.keys()

	inputhash = {}
	for geneid in geneids:
		print "Finding overlap : ", geneid
		if len(seqhash[geneid].keys()) == 1:
			inputhash["%s_onet_%s" % (geneid, seqhash[geneid].keys()[0])] = seqhash[geneid][seqhash[geneid].keys()[0]]
			continue
		common_sets, missing = overlapNoCulprit(seqhash[geneid])
		for common_set in common_sets:
			common, used = common_set
			inputhash["%s_common_%s" % (geneid, "_".join(used))] = common
		for trnsid in missing: inputhash["%s_manyt_%s" % (geneid, trnsid)] = seqhash[geneid][trnsid]

	return inputhash

def write_inputfile(inputhash, fileforinput):

	ophandle = open(fileforinput, "w")
	for key in inputhash.keys():
		ophandle.write(">%s\n" % key)
		ophandle.write("%s\n" % inputhash[key])
	ophandle.close()

	return

if __name__ == "__main__":
	
	sequencesFile, fileForInput = argv[1], argv[2]
	print "Parsing sequences"
	sequencehash = make_seqhash(sequencesFile)
	print "Determining overlaps"
	iphash = define_input(sequencehash)
	print "Writing input file for DEQOR"
	write_inputfile(iphash, fileForInput)



	


	

		
	



