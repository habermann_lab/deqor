from helpers import constructSeq
penaltyThreshold = 7 # see http://www.pubmedcentral.nih.gov/articlerender.fcgi?artid=441546&rendertype=table&id=gkh408tb1
crosssilencersAllowPercent = 0.05

def findQuartile(efficiency):

	"""
	input : 0 < number < 1
	output : which quartile of 0 - 1 it belongs to
	"""
	
	if efficiency >= 0 and efficiency <= 0.25: return 1
	elif efficiency > 0.25 and efficiency <= 0.5: return 2
	elif efficiency > 0.5 and efficiency <= 0.75: return 3
	else: return 4
	

def cmpStretch(x, y):

	"""
	input : two tuples of the format (esiRna efficient siRnas %, length of esiRna)
	output : comparison function for the in-built sort function
	"""
	
	xQuartile = findQuartile(x[0])
	yQuartile = findQuartile(y[0])

	if (x[1] < y[1]) and ((xQuartile < yQuartile) or (xQuartile == yQuartile)): return 1
	elif (x[1] < y[1]) and (xQuartile > yQuartile): return -1
	elif (x[1] > y[1]) and ((xQuartile > yQuartile) or (xQuartile == yQuartile)): return -1
	elif (x[1] > y[1]) and (xQuartile < yQuartile): return 1
	else:
		if x[0] < y[0]: return 1
		elif x[0] > y[0]: return -1
		else: return 0
	
def assignCountQualityScore(deqorOp):


    """ 
	 input : [(index,oligo,penalty incl cross silencing,penalty excl cross silencing),..]
	 output : (number of good siRNAs, number of siRNAs that cross silence, total number)
	 """
	 
    analysis = []
    total = len(deqorOp)
    for oligo in deqorOp:
        marker = 0 # 0 - when not good or bad
        if (oligo[3] <= penaltyThreshold): marker = 1 # 1 - marked efficient
        if (oligo[3] != oligo[2]) and (oligo[2] > oligo[3]): marker =  -1 # -1 - marked cross silenced
        analysis.append(marker)

    goodSilencers = analysis.count(1)
    csSilencers = analysis.count(-1)

    del(deqorOp)

    return (goodSilencers, csSilencers, total)

def analyzeStretches(deqorOp, useCrossSilencing = False):

	"""
	input : [(index,oligo,penalty incl cross silencing,penalty excl cross silencing),..]
	output : dictionary of stretches starting at each position in the esirna
	"""
	
	siRNANum = len(deqorOp)
	
	
	# Here be demons
	if siRNANum <= 250:
		sizes = range(150, 250) # magic numbers refer to the toleratable esi size
		esiMinimum = 150
	elif siRNANum > 500:
		sizes = range(400, 600, 50)
		esiMinimum = 400
	else:
		sizes = range(siRNANum - 50, siRNANum, 50)
		esiMinimum = siRNANum - 50
	

	analysis = []
	deqorOpLength = len(deqorOp)
	for start in range(deqorOpLength - esiMinimum):
		currEsi = {}
		for size in sizes:
			if (start + size) <= deqorOpLength:
				efficientCount, crosssilencersCount, total = assignCountQualityScore(deqorOp[start:start+size])
				if useCrossSilencing:
					if(crosssilencersCount <= (crosssilencersAllowPercent * total)):
						currEsi[size] = efficientCount/float(total)
					else:
						continue
						#currEsi[size] = -1
				else: currEsi[size] = efficientCount/float(total)
		analysis.append(currEsi)
	return analysis

def findBestStretch(deqorOp):

	"""
	input : dictionary of stretches starting at each position in the esirna
	output : list of sequences to use
	"""
	
	stretches = []
	stretchData = analyzeStretches(deqorOp)
	if not len(stretchData):
		stretchData = analyzeStretches(deqorOp, False)
		print "Turning off cross-silencing"
	for pos in range(len(stretchData)):
		currStretchSizesHash = stretchData[pos]
		currSizes = currStretchSizesHash.keys()
		for currSize in currSizes: stretches.append((currStretchSizesHash[currSize], currSize, pos))
	stretches.sort(cmpStretch)

	sequences = []
	
	for stretch in stretches:
		sequences.append((stretch[0], stretch[1], constructSeq(deqorOp[stretch[2]:stretch[2]+stretch[1]])))
	return sequences
		

	
		
		
	
	
	
			
			
			
			

					
				
					
				
		



	 

