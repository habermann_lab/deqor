#!/usr/bin/python

from pickle import load
import simplejson
import cgi
import os

form = cgi.FieldStorage()
id = form["processId"].value

outputdir = "/export/deqor_v3/output"
print "Content-type:text/json\n\n"
if os.path.exists(os.path.join(outputdir,id,"qualdone.flag")):
	qualities = load(open(os.path.join(outputdir,id,"qual.dump")))

	windowData = qualities["windows"]
	sirna_ids = windowData.keys()
	sirna_ids_numbers = [int(sirna_id) for sirna_id in sirna_ids]
	sirna_ids_numbers.sort()
	sirna_ids_sorted = [str(sirna_id_num) for sirna_id_num in sirna_ids_numbers]
	windowstring = "{"
	for sirna_id in sirna_ids_sorted:
		windowstring += "'%s' : " % sirna_id
		windowstring += "%s," % str(windowData[sirna_id])
	windowstring = windowstring[:-1]
	windowstring += "}"
	
	jsonstring = "{'windows':%s,'maximum':'%s','sequence':'%s'}" % (windowstring,qualities['maximum'],qualities['sequence'])
	print jsonstring

	

	#print simplejson.dumps(qualities)
else:
	print simplejson.dumps("WAIT")
