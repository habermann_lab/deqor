#!/usr/bin/python

from pickle import load
import simplejson
import cgi
import os

form = cgi.FieldStorage()
id = form["processId"].value

outputdir = "/export/deqor_v3/output"
print "Content-type:text/json\n\n"
if os.path.exists(os.path.join(outputdir,id,"offtargetsdone.flag")):
	offtargets = load(open(os.path.join(outputdir,id,"offtargets.dump")))
	
	sirna_ids = offtargets.keys()
	sirna_ids_numbers = [int(sirna_id) for sirna_id in sirna_ids]
	sirna_ids_numbers.sort()
	sirna_ids_sorted = [str(sirna_id_num) for sirna_id_num in sirna_ids_numbers]
	windowstring = "{"
	for sirna_id in sirna_ids_sorted:
		windowstring += "'%s' : " % sirna_id
		windowstring += "%s," % str(offtargets[sirna_id])
	windowstring = windowstring[:-1]
	windowstring += "}"
	
	print windowstring
	

	#print simplejson.dumps(offtargets)
else:
	print simplejson.dumps("WAIT")
