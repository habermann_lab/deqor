#!/usr/bin/python

import cgi
from time import time
from random import random
import simplejson
from pickle import dump
import os

form = cgi.FieldStorage()
id = str(int(time() * random()))

params = {}
for key in form.keys(): params[key] = form[key].value

dump_input_file = open("/tmp/deqorv2__%s.dump" % id, "w")
dump(params, dump_input_file)
dump_input_file.close()

print "Content-type:text/json\n\n"
print simplejson.dumps({"processId":id})


