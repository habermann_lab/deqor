<?php

	header('Content-type: text/json');
	
	
	
	/*****************************************
	* create json markup from array
	*****************************************/
	function create_json($data, $recursed = false) {
	
		$json = '{';
		
		foreach($data as $key => $value)
		{
			$json .= '"' . $key . '": ';
			
			if(is_array($value))
			{
				$json .= create_json($value, true);
			}
			else
			{
				$json .= '"' . $value . '",';
			}
		}
		
		$json .= '}';
		
		return $json;
	}
	
	
	
	/*****************************************
	* process input data and return the process id
	*****************************************/
	function setParams() {
		
		// process the post data...
		
		return '{"processId":"1234567890"}';
	}
	
	
	
	/*****************************************
	* return the sequence features of a process
	*****************************************/
	function getSeqFeatures($processId) {
		
		// return an array with sample data
		$base = array('A', 'C', 'G', 'T');
		$sequence = '';
		$seqLength = 1000;
		$winLength = 21;
		$maxEfficiency = 50;
		
		$seqFeatures = '{';
		
		// generate the test sequence
		// ==========================
		$seqFeatures .= '"sequence": "';
		
		for($i = 0; $i < $seqLength; $i++)
		{
			$sequence .= $base[rand(0,3)];
		}
		
		$seqFeatures .= $sequence . '", ';
		
		// generate the test maximum
		// =========================
		$seqFeatures .= '"maximum": "' . $maxEfficiency . '", ';
		
		// generate the test windows
		// =========================
		$seqFeatures .= '"windows": {';
		
		for($i = 0; $i < ($seqLength - $winLength); $i++)
		{
			$seqFeatures .= '"' . $i . '": {"sequence": "' . substr($sequence, $i, $winLength) . '", "efficiency": "' . rand(0,$maxEfficiency) . '"}, ';
		}
		
		$seqFeatures .= '}';
		
		$seqFeatures .= '}';
		
		return $seqFeatures;
	}
	
	
	
	/*****************************************
	* return the off targets of a process
	*****************************************/
	function getOffTargets($processId) {
	
	}
	
	//echo create_json($_GET);
	
	// since php 5.2.0: json_encode()
	//echo json_encode($_GET);
	
	
	
	// parse get array for function to call
	if($_GET['func'] == "setParams")
	{
		echo setParams();
	}
	else
	if($_GET['func'] == "getSeqFeatures" && $_GET['processId'] != null)
	{
		echo getSeqFeatures($_GET['processId']);
	}
	else
	if($_GET['func'] == "getOffTargets" && $_GET['processId'] != null)
	{
		echo getOffTargets($_GET['processId']);
	}
	
?>
