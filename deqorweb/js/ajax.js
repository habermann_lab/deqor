/*
 * Implementation of DEQOR's ajax functions
 */

// set the urls for the deqor functions and pages
var viewInputsUrl = "input.html";
var viewResultsUrl = "result.html";

var setParamsUrl = "cgi-bin/setParams.py";
var getSeqFeaturesUrl = "cgi-bin/getSeqFeatures.py";
var getOffTargetsUrl = "cgi-bin/getOffTargets.py";

var seqFeaturesReceived = false;
var offTargetsReceived = false;

var getArray = new Array();

var processId = "";

function init() {
	
	parseQueryString();
	updateProcessId();
}

WebElements.Utils.addLoadEvent(init);

// parse a query string (get parameters) of a url
// and store it in local array
function parseQueryString() {
	
	var querystring = location.search;
	
	if(querystring != "")
	{
		querystring = querystring.slice(1);
		
		var queryarray = querystring.split('&');
		
		for(var i = 0; i < queryarray.length; i++)
		{
			var query = queryarray[i].split('=');
			
			var key = unescape(query[0]).replace("+", " ");
			var value = unescape(query[1]).replace("+", " ");
			
			getArray[key] = value;
		}
	}
}

function updateProcessId() {
	
	processId = (getArray["processId"] != null) ? getArray["processId"] : processId;
}

function viewResults() {
	
	location.href = viewResultsUrl + "?processId=" + escape(processId);
}

function setParams() {
	
	// use dojo xhrPost to send form data via post
	dojo.xhrGet({
		url: setParamsUrl,
		// use the form parameter to send the form data
		form: "filter",
		handleAs: "json",
		timeout: 2000,
		load: function(data, ioArgs) {
			console.log(data);
			processId = data.processId;
			viewResults();
		},
		// if any error occurs, it goes here:
		error: function(error, ioArgs) {
			console.log("Error sending the form data...");
		}
	});
}

function getSeqFeatures() {
	
	if(!seqFeaturesReceived)
	{
		dojo.xhrGet({
			url: getSeqFeaturesUrl,
			handleAs: "json",
			timeout: 10000,
			// use the content parameter to set get data in json format
			content: {processId:processId, func:"getSeqFeatures"},
			load: function(data, ioArgs) {
				console.log(data);
				if(data == "WAIT")
				{
					setTimeout(function(){ getSeqFeatures(); }, 1000);
					return false;
				}
				if(data == "ERROR")
				{
					console.log("getSeqFeatures returned ERROR...");
					errorDiagram("An error occurred while calculating the sequence features.<br>Please try it again!");
					return false;
				}
				// remember state
				seqFeaturesReceived = true;
				
				// trigger rendering of diagram here
				initDiagram(data);
			},
			// if any error occurs, it goes here:
			error: function(error, ioArgs) {
				console.log("Error retrieving sequence features...");
				errorDiagram("A connection error occurred while retrieving the sequence features.<br>Please contact: <a href=\"mailto:surendra@mpi-cbg.de\">Vineeth Surendranath</a>!");
			}
		});
	}
}

function getOffTargets() {
	
	if(!offTargetsReceived)
	{
		dojo.xhrGet({
			url: getOffTargetsUrl,
			handleAs: "json",
			timeout: 10000,
			// use the content parameter to set get data in json format
			content: {processId:processId, func:"getOffTargets"},
			load: function(data, ioArgs) {
				console.log(data);
				if(data == "WAIT")
				{
					console.log("getOffTargets returned WAIT...");
					waitOffTargets();
					return false;
				}
				if(data == "ERROR") 
				{
					console.log("getOffTargets returned ERROR...");
					errorOffTargets("An error occurred while calculating the Off-Targets.<br>Please try it again!");
					return false;
				}
				// remember state
				offTargetsReceived = true;
				
				// trigger rendering of off targets here
				renderOffTargets(data);
			},
			// if any error occurs, it goes here:
			error: function(error, ioArgs) {
				console.log("Error retrieving off targets...");
				errorOffTargets("A connection error occurred while retrieving the off-targets.<br>Please contact: <a href=\"mailto:surendra@mpi-cbg.de\">Vineeth Surendranath</a>!");
			}
		});
	}
}
