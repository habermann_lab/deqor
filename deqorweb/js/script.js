var dmaxh;
var dmaxw;
var doffs;
var dyzero;

var numv;
var maxv;

var values = new Array();
var bases = new Array();
var csscores = new Array();
var basesequence;
var windowlength;

var threshhold;
var threshlow;
var threshmed;

var diagramdata;
var diagram;

// objects
var cursor;
var selector;
var sequence;
var scroller;
var zoomer;
var details;
var currentbar;
var grid;

// string
// id's of diagram and bars
var diagramId = "result-diagram";
var barId = "result-diagram-bar-";
var barClassName = "result-diagram-bar";
var barClassNameLow = "result-diagram-bar-good";
var barClassNameMed = "result-diagram-bar-medium";
var barClassNameHigh = "result-diagram-bar-bad";
var barClassNameCurr = "result-diagram-bar-highlight";

// id's of details
var detailsWindowId = "result-details-item-window";
var detailsEfficiencyId = "result-details-item-efficiency";
var detailsSeqRangeId = "result-details-item-seqrange";
var detailsSeqLenghtId = "result-details-item-seqlength";
var detailsSeqEfficiencyId = "result-details-item-seqefficiency";
var detailsSeqCrossSilencesId = "result-details-item-seqcrosssilences";

// id's of controls
var scrollerId = "result-scroller";
var zoomValueId = "result-zoom-value";
var zoomDecreaseId = "result-zoom-decrease";
var zoomIncreaseId = "result-zoom-increase";
var selectionId = "result-diagram-selection";
var gridToggleId = "result-grid-toggle";
var diagramStatusId = "result-diagram-status";

var sequenceWindowId = "sequence";

// id's of off-targets
var offTargetsTableId = "offtargets-table";
var offTargetsStatusId = "offtargets-status";
var offTargetsTriggerId = "offtargets-trigger";



// set up diagram and all additional functionality 
// for displaying the results
function initDiagram(data) {
	
	// prepare the result data
	
	// copy the result data
	diagramdata = data;
	
	// move data into script variables 
	// (more flexible when changing the data object at some point)
	basesequence = diagramdata.sequence;
	
	windowlength = diagramdata.windows[0].sequence.length;
	
	// efficiencies and bases
	for(var i in diagramdata.windows)
	{
		values.push(parseInt(diagramdata.windows[i].efficiency));
		csscores.push(parseInt(diagramdata.windows[i].efficiency));
		bases.push(diagramdata.windows[i].sequence.substring(0,1));
	}
	
	// number of values (of windows)
	// numv = parseInt(i) + 1;
	numv = values.length;
	
	// maximum value (max efficiency)
	maxv = diagramdata.maximum;
	
	// prepare the diagram
	dmaxh = 150;
	
	// threshholds
	threshlow = 7;
	threshmed = 15;
	threshhold = 5;
	
	// selector object for selecting window ranges
	selector = new Object();
	selector.object = document.getElementById(selectionId);
	selector.objectValue = document.getElementById(detailsSeqRangeId);
	if(selector.objectValue.firstChild == null)
	{
			var textnode = document.createTextNode("");
			selector.objectValue.appendChild(textnode);
			selector.objectValue = textnode;
	}
	else selector.objectValue = selector.objectValue.firstChild;
	selector.active = false;
	selector.start = 0;
	selector.end = 0;
	
	// sequence window below the diagram
	sequence = new Object();
	sequence.objectValue = document.getElementById(sequenceWindowId);
	if(sequence.objectValue.firstChild == null)
	{
			var textnode = document.createTextNode("");
			sequence.objectValue.appendChild(textnode);
			sequence.objectValue = textnode;
	}
	else sequence.objectValue = sequence.objectValue.firstChild;
	sequence.curr = "";
	
	// scroller object for remembering scroll offset of diagram and proper highlighting
	scroller = new Object();
	scroller.object = document.getElementById(scrollerId);
	/*
	scroller.objectValue = document.getElementById(detailsScrollId);
	if(scroller.objectValue.firstChild == null)
	{
			var textnode = document.createTextNode("");
			scroller.objectValue.appendChild(textnode);
			scroller.objectValue = textnode;
	}
	else scroller.objectValue = scroller.objectValue.firstChild;
	*/
	scroller.amount = 0;
	
	// zoomer object for allowing to zoom in diagram
	zoomer = new Object();
	zoomer.objectIn = document.getElementById(zoomIncreaseId);
	zoomer.objectOut = document.getElementById(zoomDecreaseId);
	zoomer.objectValue = document.getElementById(zoomValueId);
	if(zoomer.objectValue.firstChild == null)
	{
			var textnode = document.createTextNode("");
			zoomer.objectValue.appendChild(textnode);
			zoomer.objectValue = textnode;
	}
	else zoomer.objectValue = zoomer.objectValue.firstChild;
	zoomer.w = 2;
	zoomer.h = 2;
	zoomer.maxw = 5;
	zoomer.maxh = 5;
	zoomer.amount = 200;
	
	cursor = new Object();
	cursor.x = 0;
	cursor.y = 0;
	cursor.v = 0;
	
	// grid object for displaying grid lines (with zooming)
	grid = new Object();
	grid.visible = false;
	grid.objectToggle = document.getElementById(gridToggleId);
	grid.y = 10;
	grid.yZoom = 0;
	grid.objectY = new Array();
	grid.x = 50;
	grid.xZoom = 2;
	grid.objectX = new Array();
	
	diagram = document.getElementById(diagramId);
	
	// calculate position/offset of the diagram 
	doffs = new Object();
	doffs.x = 0;
	doffs.y = 0;
	
	elem = diagram;
	
	while(elem)
	{
		doffs.x += elem.offsetLeft;
		doffs.y += elem.offsetTop;
		elem = elem.offsetParent;
	}
	
	// fix problem with borders, that are not calculated into offset --> mouse will point wrong if not fixed
	doffs.x += 2;	// 2px border width must be added
	doffs.y += 2;	// 2px border width must be added
	
	// in IE there are 4px offset --> strange as always
	if(WebElements.Utils.getBrowserName() == "Microsoft Internet Explorer")
	{
		doffs.x += 2;	// 2px additional border width must be added
		doffs.y += 2;	// 2px additional border width must be added
	}
	
	// offset of diagrams x-axis (y = 0)
	dyzero = 30;
	
	// add event listeners to components
	WebElements.Utils.addEventListener(zoomer.objectOut, "click", zoomOut);
	WebElements.Utils.addEventListener(zoomer.objectIn, "click", zoomIn);
	WebElements.Utils.addEventListener(scroller.object, "scroll", updateScroll);
	WebElements.Utils.addEventListener(grid.objectToggle, "click", toggleGrid);
	WebElements.Utils.addEventListener(diagram, "mousemove", function(e) { updateDiagram(e); });
	WebElements.Utils.addEventListener(diagram, "mousedown", function(e) { startSelect(e); });
	WebElements.Utils.addEventListener(diagram, "mouseup", function(e) { endSelect(e); });
	
	// render diagram
	renderResultDiagram();
	
	// update zoom
	updateZoom();
	
	// init current bar -> take the first one
	currentbar = new Object();
	currentbar.object = document.getElementById(barId + "0");
	currentbar.cname = currentbar.object.className;
	currentbar.value = 0;
}



// function to generate random sequences
// and efficiencies for testing purposes
// this function is not used anymore
function getResultValues() {
	
	// generate random values for testing
	for(var i = 0; i < numv; i++)
	{
		values[i] = parseInt(Math.random() * maxv);
	}
	
	// generate random bases for testing
	for(var k = 0; k < numv; k++)
	{
		var letter = parseInt(Math.random() * 4);
		
		switch(letter)
		{
			case 0:
					bases[k] = "A";
					break;
			case 1:
					bases[k] = "C";
					break;
			case 2:
					bases[k] = "G";
					break;
			case 3:
					bases[k] = "T";
					break;
		} 
	}
}


// display error in diagram panel
function errorDiagram(message) {
	
	var status = document.getElementById(diagramStatusId);
	
	if(status)
	{
		status.className = status.className.replace("wait", "");
		status.className += " error";
		status.innerHTML = message;
	}
}

// remove wait state from diagram panel
function successDiagram() {
	
	var status = document.getElementById(diagramStatusId);
	
	if(status)
	{
		diagram.removeChild(status);
	}
}



// this function renders the diagram
function renderResultDiagram() {

	// remove wait state from diagram panel
	successDiagram();
	
	// initial css setup for diagram panel
	diagram.style.position = "relative";
	diagram.style.width = (numv * zoomer.w) + "px";
	
	for(var i = 0; i < numv; i++)
	{
		var bar = document.createElement("div");
		
		bar.id = barId + i;
		
		// map value to color by threshold
		if(values[i] <= threshhold)
		{
			bar.className = barClassNameLow;
		}
		/*	
		else if(values[i] == crosssilencing)
		{
			bar.className = barClassNameMed;
		}
		*/
		else bar.className = barClassNameHigh;
		
		// map value to height
		bar.style.height = (values[i] * zoomer.h) + "px";
		bar.style.width = (zoomer.w) + "px";
		bar.style.position = "absolute";
		bar.style.bottom = dyzero + "px";
		bar.style.left = (i * zoomer.w) + "px";
		
		diagram.appendChild(bar);
	}
	
	// render the grid
	renderGrid();
}



// this function refreshes the diagram 
// after changing of zoom value
function refreshResultDiagram() {
	
	// recalculate width of the diagram panel
	diagram.style.width = (numv * zoomer.w) + "px";
	
	// recalculate width and position of the bars
	for(var i = 0; i < numv; i++)
	{
		var bar = document.getElementById(barId + i);
		
		// map value to color by threshold
		if (csscores[i] > values[i])
		{
			bar.className = barClassNameMed;
		}
		else if(values[i] <= threshhold)
		{
			bar.className = barClassNameLow;
		}
		else bar.className = barClassNameHigh;
		/*
		if (csscores[i] > values[i])
		{
			bar.className = barClassNameMed;
		}
		*/
		// map value to height
		bar.style.height = (values[i] * zoomer.h) + "px";
		bar.style.width = (zoomer.w) + "px";
		bar.style.left = (i * zoomer.w) + "px";
	}
	
	// refresh the selection with new zoom value
	renderSelect();
	
	// refresh the grid with new zoom value
	refreshGrid();
}



// this function renders the grid lines
function renderGrid() {
	
	for(var i = 0; i <= (parseInt(maxv/grid.y) + 1); i++)
	{
		var gridline = document.createElement("div");
		gridline.className = "gridline-horizontal";
		gridline.style.height = "1px";
		gridline.style.width = "100%";
		gridline.style.zIndex = "0";
		gridline.style.position = "absolute";
		gridline.style.display = (grid.visible) ? "" : "none";
		gridline.style.bottom = (dyzero - 1 + (grid.y * i * zoomer.h)) + "px";
		
		grid.objectY[i] = gridline;
		
		diagram.appendChild(gridline);
	}
	
	for(var k = 0; k <= (parseInt(numv/grid.x)); k++)
	{
		var gridline = document.createElement("div");
		gridline.className = "gridline-vertical";
		gridline.style.height = "5px";
		gridline.style.width = "1px";
		gridline.style.zIndex = "0";
		gridline.style.position = "absolute";
		gridline.style.display = (grid.visible) ? "" : "none";
		gridline.style.bottom = (dyzero - 5) + "px";
		gridline.style.left = ((grid.x * k * zoomer.w)) + "px";
		
		grid.objectX[k] = gridline;
		
		diagram.appendChild(gridline);
	}
}



// this function refreshes the grid lines
// after changing the zoom value
function refreshGrid() {
	
	for(var i = 0; i < grid.objectY.length; i++)
	{
		grid.objectY[i].style.bottom = (dyzero - 1 + (grid.y * i * zoomer.h)) + "px";
	}
	
	for(var k = 0; k < grid.objectX.length; k++)
	{
		grid.objectX[k].style.left = ((grid.x * k * zoomer.w)) + "px";
	}
}



// turn grid visibility on and off
function toggleGrid() {
	
	if(grid.visible)
	{
		for(var i = 0; i < grid.objectY.length; i++)
		{
			grid.objectY[i].style.display = "none";
		}
		
		for(var k = 0; k < grid.objectX.length; k++)
		{
			grid.objectX[k].style.display = "none";
		}
		
		grid.visible = false;
		grid.objectToggle.className = grid.objectToggle.className.replace("-active", "");
	}
	else
	{
		for(var i = 0; i < grid.objectY.length; i++)
		{
			grid.objectY[i].style.display = "";
		}
		
		for(var k = 0; k < grid.objectX.length; k++)
		{
			grid.objectX[k].style.display = "";
		}
		
		grid.visible = true;
		grid.objectToggle.className = grid.objectToggle.className + "-active";
	}
}



function zoomOut() {
	
	if(zoomer.w > 1 && zoomer.w >= zoomer.h) zoomer.w--;
	if(zoomer.h > 1 && zoomer.h > zoomer.w) zoomer.h--;
	
	updateZoom();
	
	refreshResultDiagram();
}



function zoomIn() {
	
	if(zoomer.w < zoomer.maxw) zoomer.w++;
	if(zoomer.h < zoomer.maxh) zoomer.h++;
	
	if(zoomer.w * numv > dmaxw) zoomer.w--;
	if(zoomer.h * maxv > dmaxh) zoomer.h--;
	
	updateZoom();
	
	refreshResultDiagram();
}



function updateZoom() {
	
	// calculate the zoom amount in percent
	zoomer.amount = zoomer.w * 100;
	
	zoomer.objectValue.data = zoomer.amount + "%";
}



function updateScroll() {
	
	// get scroll offset of the diagram panel
	scroller.amount = scroller.object.scrollLeft;
	
	//scroller.objectValue.data = scroller.amount;
}



function startSelect(e) {
	
	// save the start position of the selection (the number of the bar)
	selector.start = cursor.v;
	selector.active = true;
	
	// clear sequence
	clearSequence();
}



function endSelect(e) {
	
	// save the end position of the selection (the number of the bar)
	selector.end = cursor.v;
	selector.active = false;
	
	// update Selection
	renderSelect();
}



// render the current selection
function renderSelect() {
	
	// render selection if the range is larger than 0
	if(selector.start != selector.end)
	{
		var start = selector.start;
		var end = selector.end;
		
		if(selector.start > selector.end)
		{
			start = selector.end;
			end = selector.start;
		}
		
		// consider zoom-factor and map selection range to width and position of the selection box 
		selector.object.style.left = (start * zoomer.w) + "px";
		selector.object.style.width = ((end - start + 1) * zoomer.w) + "px";
		
		selector.objectValue.data = start + " - " + end;
		
		// update the sequence
		updateSequence();
	}
	else
	{
		selector.object.style.width = "";
		selector.object.style.left = "";
		
		selector.objectValue.data = "";
		
		// clear sequence
		clearSequence();
	}
}



// update the sequence window with current selection
function updateSequence() {
	
	var start = selector.start;
	var end = selector.end;
	
	if(selector.start > selector.end)
	{
		start = selector.end;
		end = selector.start;
	}
	
	sequence.curr = "";
	
	for(var i = start; i <= end; i++)
	{
		sequence.curr += bases[i];
	}
	sequence.curr += basesequence.substr((end + 1),(windowlength - 1));
	
	sequence.objectValue.data = "Windows: " + start + " - " + end + "\n" + sequence.curr;
}



// clear the sequence window
function clearSequence() {
	
	sequence.curr = "";
	sequence.objectValue.data = "";
}



// update diagram display on mouse movement
function updateDiagram(e) {
	
	// get position of the mouse
	var mpos = WebElements.Utils.getMousePosition(e);
	
	// calculate position of the mouse relative to the diagram panel
	cursor.x = mpos.x - doffs.x + scroller.amount;
	cursor.y = mpos.y - doffs.y;
	
	// calculate the bar (-number), that is underneath the mouse pointer
	cursor.v = parseInt(cursor.x / zoomer.w);
	
	// select the bar, that is underneath the mouse pointer
	var newbar = document.getElementById(barId + cursor.v);
	
	if(newbar != null)
	{
		// reset old bar and setup the new bar as current bar to highlight it
		currentbar.object.className = currentbar.cname;
		currentbar.object = newbar;
		currentbar.cname = newbar.className;
		currentbar.value = cursor.v;
		currentbar.object.className = barClassNameCurr;
	}
	
	// change selection range if mouse is held down
	if(selector.active)
	{
		selector.end = cursor.v;
		
		// render selection
		renderSelect();
	}
	
	// update details
	updateDetails();
}



// update and display information on current selection in diagram
function updateDetails() {
	
	// fill result details with text
	var windowtext = document.getElementById(detailsWindowId).firstChild;
	if(windowtext == null)
	{
		windowtext = document.createTextNode("");
		document.getElementById(detailsWindowId).appendChild(windowtext);
	}
	windowtext.data = cursor.v;
	
	var qualitytext = document.getElementById(detailsEfficiencyId).firstChild;
	if(qualitytext == null)
	{
		qualitytext = document.createTextNode("");
		document.getElementById(detailsEfficiencyId).appendChild(qualitytext);
	}
	qualitytext.data = values[cursor.v];
	
	var seqlengthtext = document.getElementById(detailsSeqLenghtId).firstChild;
	if(seqlengthtext == null)
	{
		seqlengthtext = document.createTextNode("");
		document.getElementById(detailsSeqLenghtId).appendChild(seqlengthtext);
	}
	seqlengthtext.data = getSelectionLength();
	
	
	var seqefficiencytext = document.getElementById(detailsSeqEfficiencyId).firstChild;
	if(seqefficiencytext == null)
	{
		seqefficiencytext = document.createTextNode("");
		document.getElementById(detailsSeqEfficiencyId).appendChild(seqefficiencytext);
	}
	seqefficiencytext.data = getSelectionEfficiency();
	
	var seqcrosssilencestext = document.getElementById(detailsSeqCrossSilencesId).firstChild;
	if(seqcrosssilencestext == null)
	{
		seqcrosssilencestext = document.createTextNode("");
		document.getElementById(detailsSeqCrossSilencesId).appendChild(seqcrosssilencestext);
	}
	seqcrosssilencestext.data = getSelectionCrossSilences();
}



// calculate length of selected window sequence
function getSelectionLength() {
	
	return (selector.end > selector.start) ? (selector.end - selector.start + 1) : (selector.start - selector.end + 1);
}



// calculate the percentage of efficient windows in selected window sequence
function getSelectionEfficiency() {
	
	var goodefficiency = 0;
	var start = selector.start;
	var end = selector.end;
	
	if(selector.start > selector.end)
	{
		start = selector.end;
		end = selector.start;
	}
	
	var length = end - start + 1; 
	
	for(var i = start; i <= end; i++)
	{
		if(values[i] <= threshhold)
		{
			goodefficiency++;
		}
	}
	
	return ((goodefficiency / length) * 100).toFixed(2);
}



// calculate the percentage of cross silences in selected window sequence
function getSelectionCrossSilences() {
	
	var crosssilences = 0;
	var start = selector.start;
	var end = selector.end;
	
	if(selector.start > selector.end)
	{
		start = selector.end;
		end = selector.start;
	}
	
	var length = end - start + 1; 
	
	for(var i = start; i <= end; i++)
	{
		if(csscores[i] > values[i])
		{
			crosssilences++;
		}
	}
	
	return ((crosssilences / length) * 100).toFixed(2);
}



// render the off targets into result table
function renderOffTargets(data) {
	
	var table = document.getElementById("offtargets-table");
	var tr;
	var items = "";
	
	for(var i in data)
	{
		csscores[i] = parseInt(data[i].csscore);
		if (csscores[i] > values[i])
		{
			tr = table.insertRow(table.rows.length);
			tr.insertCell(0).innerHTML = i;
			tr.insertCell(1).innerHTML = data[i].csscore;
		
			for(k in data[i].ids)
			{	
				items += "<a class=\"transcript\" href=\"http://www.ncbi.nlm.nih.gov/nuccore/" + data[i].ids[k] + "\" target=\"_blank\">" + data[i].ids[k] + "</a>";
			}	
		
			k = parseInt(k) + 1;
		
			tr.insertCell(2).innerHTML = "<div class=\"dropdown\"><div class=\"count dropdownheader\">Show all " + k + 
									 "</div><div class=\"items dropdownbody\">" +
									 items + "</div></div>";
									 
			items = "";
			k = -1;
		
			addDropDownBox(tr.cells[2].firstChild);
		}
	}
	
	successOffTargets();
	refreshResultDiagram();
}

// display error in off-targets table
function errorOffTargets(message) {
	
	var status = document.getElementById(offTargetsStatusId);
	
	if(status)
	{
		status.className = status.className.replace("wait", "");
		status.className += " error";
		status.innerHTML = message;
	}
}

// remove wait state from off-targets table
function successOffTargets() {
	
	var status = document.getElementById(offTargetsStatusId);
	
	if(status)
	{
		status.className = status.className.replace("wait", "");
		status.className = status.className.replace("error", "");
		status.innerHTML = "Off-Targets successfully received.";
	}
	
	var trigger = document.getElementById("offTargetsTriggerId");
	
	if(trigger)
	{
		trigger.parentNode.removeChild(trigger);
	}
}

// display wait state in off-targets table
function waitOffTargets() {
	
	var status = document.getElementById(offTargetsStatusId);
	
	if(status)
	{
		status.className += " wait";
	}
}
