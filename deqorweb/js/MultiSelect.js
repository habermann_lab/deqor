
// MultiSelect Class
// Widget for a user interface, that allows editing of multiple values for each value groups

MultiSelect = function(element, options) {
	
	this.element = element;					// element that contains the value groups
	this.editor = null;						// the editor panel for editing multiple values
	this.items = null;						// internal list for value groups
	
	this.items_tagname = "div";				// tag name to parse for value groups
	this.items_classname = "item";			// tag name to parse for value groups
	
	this.item_current = 0;					// most recently edited value group
	this.item_current_state = 0;			// state of the most recent value group (0 == not active | 1 == active/ currently edited)
	
	this.editor_inputs;						// inputs of the editor form
	this.formname = "";						// name of the general form
	
	this.setOptions(options);
	
	this.init();
}

MultiSelect.prototype.setOptions = function(options) {
	
	if(options) 
	{
		for(var optionname in options)
		{
			this[optionname] = options[optionname];
		}
		
		return true;
	}
	
	return false;
}

MultiSelect.prototype.init = function() {
	
	if(this.editor == null)
	{
		var tempeditor = this.element.firstChild;
		
		while(tempeditor && tempeditor.nodeType != 1)
		{
			tempeditor = tempeditor.nextSibling;
		}
		
		this.editor = new MultiSelectEditor(tempeditor, this, {control_reset:WebElements.Utils.getElementsByClassName(this.element,"div","editpanel-reset")[0], control_close:WebElements.Utils.getElementsByClassName(this.element,"div","editpanel-close")[0]});
	}
	
	if(this.items == null)
	{
		this.items = new Array();
		
		var tempitems = WebElements.Utils.getElementsByClassName(this.element, this.items_tagname, this.items_classname);
		
		for(var i = 0; i < tempitems.length; i++)
		{
			this.items.push(new MultiSelectItem(tempitems[i], i, this));
		}
	}
}

MultiSelect.prototype.notifyActive = function(index) {
	
	if(this.item_current_state == 0)
	{
		this.item_current = index;
		this.item_current_state = 1;
		this.items[index].setActive();
		
		this.openEditor(index);
	}
	else
	{
		this.closeEditor();
		
		this.items[this.item_current].setInactive();
		this.item_current = index;
		this.item_current_state = 1;
		this.items[index].setActive();
		
		this.openEditor(index);
	}
}

MultiSelect.prototype.notifyInactive = function(index) {
	
	this.item_current = index;
	this.item_current_state = 0;
	this.items[index].setInactive();
	
	this.closeEditor();
}

MultiSelect.prototype.openEditor = function(index) {
	
	this.editor.open();
}

MultiSelect.prototype.closeEditor = function() {
	
	this.editor.close();
	this.editor.reset();
}



// MultiSelectEditor Class
// defines handling of editor input elements

MultiSelectEditor = function(element, parent, options) {
	
	this.element = element;
	this.parent = parent;
	this.inputs = null;
	
	this.control_reset = null;		// html element that triggers editor reset
	this.control_close = null;		// html element that triggers editor close
	
	this.callback_open = null;		// allows adding of callback functions to editor opening for higher customization
	this.callback_close = null;		// allows adding of callback functions to editor closing for higher customization
	this.callback_reset = null;		// allows adding of callback functions to editor reseting for higher customization
	
	this.setOptions(options);
	
	this.init();
	
	this.setupFormControl();
}

MultiSelectEditor.prototype.setOptions = function(options) {
	
	if(options) 
	{
		for(var optionname in options)
		{
			this[optionname] = options[optionname];
		}
		
		return true;
	}
	
	return false;
}

MultiSelectEditor.prototype.init = function() {
	
	this.inputs = new Array();
	
	var temp = this.element.getElementsByTagName("input");
	
	for(var i = 0; i < temp.length; i++)
	{
		this.inputs.push(temp[i]);
	}
	
	var temp = this.element.getElementsByTagName("select");
	
	for(var i = 0; i < temp.length; i++)
	{
		this.inputs.push(temp[i]);
	}
	
	var temp = this.element.getElementsByTagName("textarea");
	
	for(var i = 0; i < temp.length; i++)
	{
		this.inputs.push(temp[i]);
	}
	
	var self = this;
	
	if(this.control_reset != null)
	{
		WebElements.Utils.addEventListener(this.control_reset, "click", function(){ self.reset(); });
	}
	
	if(this.control_close != null)
	{
		WebElements.Utils.addEventListener(this.control_close, "click", function(){ self.parent.notifyInactive(self.parent.item_current); });
	}
}

MultiSelectEditor.prototype.setCallback = function(callback, routine) {
	
	if(callback != null && typeof callback == "function")
	{
		if(routine == "open")
		{
			this.callback_open = callback;
			return true;
		}
		if(routine == "close")
		{
			this.callback_close = callback;
			return true;
		}
		if(routine == "reset")
		{
			this.callback_reset = callback;
			return true;
		}
	}
	
	return false;
}

MultiSelectEditor.prototype.addCallback = function(callback, routine) {
	
	if(callback != null && typeof callback == "function")
	{
		if(routine == "open")
		{
			var oldcallback = this.callback_open;
			
			if(this.callback_open != null && typeof this.callback_open == "function")
			{
				this.callback_open = function() { oldcallback(); callback(); };
			}
			else 
			{
				this.callback_open = callback;
			}
			return true;
		}
		if(routine == "close")
		{
			var oldcallback = this.callback_close;
			
			if(this.callback_close != null && typeof this.callback_close == "function")
			{
				this.callback_close = function() { oldcallback(); callback(); };
			}
			else 
			{
				this.callback_close = callback;
			}
			return true;
		}
		if(routine == "reset")
		{
			var oldcallback = this.callback_reset;
			
			if(this.callback_reset != null && typeof this.callback_reset == "function")
			{
				this.callback_reset = function() { oldcallback(); callback(); };
			}
			else 
			{
				this.callback_reset = callback;
			}
			return true;
		}
	}
	
	return false;
}

// This function has to be setup manually in each instance to customize the editor (e.g. adding ButtonSets and adding callbacks)
MultiSelectEditor.prototype.setupFormControl = function() {
	
	var buttonset = new ButtonSet(WebElements.Utils.getElementsByClassName(document.getElementById("pospref"), "div", "buttonset")[0]);

	this.addCallback(function(){buttonset.update();}, "open");
	this.addCallback(function(){buttonset.reset();}, "reset");
	this.addCallback(function(){buttonset.reset();}, "close");
	
	var self = this;
	
	var posPrefItemCallback = function() {
		
		var pospref = self.parent.items[self.parent.item_current].element.getElementsByTagName("span")[0];
/* 		var penalty = self.parent.items[self.parent.item_current].element.getElementsByTagName("span")[1]; */
		
		var pospref_not = self.parent.items[self.parent.item_current].inputs[1].value;
		var pospref_value = self.parent.items[self.parent.item_current].inputs[0].value;
/* 		var penalty_value = self.parent.items[self.parent.item_current].inputs[2].value; */
		
		if(pospref.firstChild)
		{
			pospref.firstChild.nodeValue = pospref_value;
			
			if(pospref_not == "not")
			{
				pospref.className += " not";
			}
			else
			{
				pospref.className = pospref.className.replace("not", "");
			}
		}
		else
		{
			pospref.appendChild(document.createTextNode(pospref_value));
			
			if(pospref_not == "not")
			{
				pospref.className += " not";
			}
			else
			{
				pospref.className = pospref.className.replace("not", "");
			}
		}
		
/*
		if(penalty.firstChild)
		{
			penalty.firstChild.nodeValue = penalty_value;
		}
		else
		{
			penalty.appendChild(document.createTextNode(penalty_value));
		}
*/
	}
	
	this.addCallback(posPrefItemCallback, "close");
}

MultiSelectEditor.prototype.open = function() {
	
	for(var i = 0; i < this.inputs.length; i++)
	{
		if(this.inputs[i].type == "checkbox")
		{
			if(document.forms[this.parent.formname].elements[this.inputs[i].name + this.parent.item_current].value != "") this.inputs[i].checked = "checked";
		}
		else
		{
			this.inputs[i].value = document.forms[this.parent.formname].elements[this.inputs[i].name + this.parent.item_current].value;
		}
	}
	
	this.element.style.left = this.parent.items[this.parent.item_current].element.offsetLeft + "px";
	this.element.className += " active";
	
	if(this.callback_open != null) this.callback_open();
}

MultiSelectEditor.prototype.close = function() {
	
	for(var i = 0; i < this.inputs.length; i++)
	{
		if(this.inputs[i].type == "checkbox")
		{
			if(this.inputs[i].checked)
			{
				document.forms[this.parent.formname].elements[this.inputs[i].name + this.parent.item_current].value = this.inputs[i].value;
			}
			else
			{
				document.forms[this.parent.formname].elements[this.inputs[i].name + this.parent.item_current].value = "";
			}
		}
		else
		{
			document.forms[this.parent.formname].elements[this.inputs[i].name + this.parent.item_current].value = this.inputs[i].value;
		}
	}
	
	this.element.className = this.element.className.replace("active", "");
	
	if(this.callback_close != null) this.callback_close();
}

MultiSelectEditor.prototype.reset = function() {
	
	for(var i = 0; i < this.inputs.length; i++)
	{
		if(this.inputs[i].type == "checkbox")
		{
			if(this.inputs[i].checked) this.inputs[i].checked = false;
		}
		else
		{
			this.inputs[i].value = "";
		}
	}
	
	if(this.callback_reset != null) this.callback_reset();
}



// MultiSelectItem Class

MultiSelectItem = function(element, index, parent, options) {
	
	this.element = element;
	this.index = index;
	this.parent = parent;
	
	this.inputs = null;
	
	this.state = 0;
	
	this.setOptions(options);
	
	this.init();
}

MultiSelectItem.prototype.setOptions = function(options) {
	
	if(options) 
	{
		for(var optionname in options)
		{
			this[optionname] = options[optionname];
		}
		
		return true;
	}
	
	return false;
}

MultiSelectItem.prototype.init = function() {
	
	var self = this;
	
	this.inputs = new Array();
	
	temp = this.element.getElementsByTagName("input");
	for(var i = 0; i < temp.length; i++) this.inputs.push(temp[i]);
	
	temp = this.element.getElementsByTagName("select");
	for(var i = 0; i < temp.length; i++) this.inputs.push(temp[i]);
	
	temp = this.element.getElementsByTagName("textarea");
	for(var i = 0; i < temp.length; i++) this.inputs.push(temp[i]);
	
	WebElements.Utils.addEventListener(this.element, "click", function(){ self.toggle(); });
}

MultiSelectItem.prototype.getInputs = function() {
	
	return this.inputs;
}

MultiSelectItem.prototype.getInput = function(name) {
	
	for(var i = 0; i < this.inputs.length; i++)
	{
		if(this.inputs[i].name == name + this.index)
		{
			return this.inputs[i];
		}
	}
	
	return false;
}

MultiSelectItem.prototype.toggle = function() {
	
	if(this.state == 0)
	{
		this.parent.notifyActive(this.index);
	}
	else
	{
		this.parent.notifyInactive(this.index);
	}
}

MultiSelectItem.prototype.setActive = function() {
	
	this.state = 1;
	this.element.className += " active";
}

MultiSelectItem.prototype.setInactive = function() {
	
	this.state = 0;
	this.element.className = this.element.className.replace("active", "");
}



//ButtonSet Class
//maps a set of buttons to a hidden input element

ButtonSet = function(element, options) {
	
	this.element = element;
	this.buttons = null;
	this.input = null;
	
	this.buttons_tagname = "div";
	this.buttons_classname = "button";
	this.buttons_active_classname = "active";
	
	this.multiple = false;
	
	this.setOptions(options);
	
	this.init();
}

ButtonSet.prototype.setOptions = function(options) {
	
	if(options) 
	{
		for(var optionname in options)
		{
			this[optionname] = options[optionname];
		}
		
		return true;
	}
	
	return false;
}

ButtonSet.prototype.init = function() {
	
	this.buttons = new Array();
	
	var temp = WebElements.Utils.getElementsByClassName(this.element, this.buttons_tagname, this.buttons_classname);
	
	for(var i = 0; i < temp.length; i++)
	{
		this.buttons.push(new Button(temp[i], i, this));
	}
	
	this.input = this.element.getElementsByTagName("input")[0];
}

ButtonSet.prototype.notifyActive = function(index) {
	
	if(!this.multiple)
	{
		this.reset();
		this.input.value = this.buttons[index].value;
		this.buttons[index].setActive();
	}
}

ButtonSet.prototype.notifyInactive = function(index) {
	
	if(!this.multiple)
	{
		this.input.value = "";
		this.buttons[index].setInactive();
	}
}

ButtonSet.prototype.reset = function() {
	
	for(var i = 0; i < this.buttons.length; i++)
	{
		if(this.buttons[i].state == 1) this.buttons[i].setInactive();
	}
}

ButtonSet.prototype.update = function() {
	
	for(var i = 0; i < this.buttons.length; i++)
	{
		if(!this.multiple)
		{
			if(this.buttons[i].value == this.input.value)
			{
				this.buttons[i].setActive();
				return;
			}
		}
	}
}



//Button Class
//single button of a ButtonSet object

Button = function(element, index, parent, options) {
	
	this.element = element;
	this.parent = parent;
	this.index = index;
	
	this.state = 0;
	this.value = "";
	
	this.active_classname = "active";
	
	this.setOptions(options);
	
	this.init();
}

Button.prototype.setOptions = function(options) {
	
	if(options) 
	{
		for(var optionname in options)
		{
			this[optionname] = options[optionname];
		}
		
		return true;
	}
	
	return false;
}

Button.prototype.init = function() {
	
	var self = this;
	
	this.value = this.element.firstChild.nodeValue;
	
	WebElements.Utils.addEventListener(this.element, "click", function(){ self.toggle(); });
}

Button.prototype.toggle = function() {
	
	if(this.state == 0)
	{
		this.parent.notifyActive(this.index);
	}
	else
	{
		this.parent.notifyInactive(this.index);
	}
}

Button.prototype.setActive = function() {
	
	this.state = 1;
	this.element.className += (" " + this.active_classname);
}

Button.prototype.setInactive = function() {
	
	this.state = 0;
	this.element.className = this.element.className.replace(this.active_classname, "");
}