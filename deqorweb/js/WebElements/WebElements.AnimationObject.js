// Element with properties that can be animated

var WebElements;

if(!WebElements) WebElements = {};

WebElements.AnimationObject = function(element, property, from, to, unit, duration, callback, form) {
	
	this.element = element;
	this.property = property;
	this.from = from;
	this.to = to;
	this.unit = unit;
	this.duration = duration;
	this.progress = 0;
	
	this.callback = null;
	this.setCallback(callback);
	
	this.transition = null;
	this.setTransition(form);
}

WebElements.AnimationObject.prototype.setCallback = function(callback) {
	
	if(callback != null && typeof callback == "function")
	{
		this.callback = callback;
		
		return true;
	}
	
	return false;
}

WebElements.AnimationObject.prototype.setTransition = function(form) {
	
	if(this.duration != null && this.duration > 0)
	{
		if(form != null && typeof form == "string")
		{
			this.transition = new WebElements.Transition(this.from, this.to, form);
			
			return true;
		}
		else
		{
			this.transition = new WebElements.Transition(this.from, this.to, "linear");
			
			return true;
		}
	}
	
	return false;
}

WebElements.AnimationObject.prototype.updateProgress = function(current_duration) {
	
	var progress = (this.duration > 0) ? (current_duration / this.duration) : 1;
	
	this.progress = (progress >= 1) ? 1 : progress; 
}

WebElements.AnimationObject.prototype.getProgress = function() {
	
	return this.progress;
}

WebElements.AnimationObject.prototype.getValue = function() {
	
	if(this.transition != null)
	{
		return (this.transition.getValueAt(this.progress) + this.unit);
	}
	else
	{
		return (this.to + this.unit);
	}
}