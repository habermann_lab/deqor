// Group of AnimationObjects to be animated parallel

var WebElements;

if(!WebElements) WebElements = {};

WebElements.AnimationGroup = function(animation_objects, callback) {
	
	this.animation_objects = new Array();
	this.animation_objects_running = new Array();
	
	if(animation_objects != null && typeof animation_objects.length != "undefined")
	{
		for(var i = 0; i < animation_objects.length; i++)
		{
			this.animation_objects.push(animation_objects[i]);
			this.animation_objects_running.push(false);
		}
	}
	else if(animation_objects != null)
	{
		this.animation_objects.push(animation_objects);
		this.animation_objects_running.push(false);
	}
	
	this.callback = null;
	this.setCallback(callback);
}

WebElements.AnimationGroup.prototype.setCallback = function(callback) {
	
	if(callback != null && typeof callback == "function")
	{
		this.callback = callback;
		
		return true;
	}
	
	return false;
}

WebElements.AnimationGroup.prototype.getAnimationCount = function() {
	
	return this.animation_objects.length;
}

WebElements.AnimationGroup.prototype.addAnimationObject = function(animation_object) {
	
	this.animation_objects.push(animation_object);
	this.animation_objects_running.push(false);
}

WebElements.AnimationGroup.prototype.getAnimationObject = function(index) {
	
	return this.animation_objects[index];
}

WebElements.AnimationGroup.prototype.activateAnimationObject = function(index) {
	
	this.animation_objects_running[index] = true;
}

WebElements.AnimationGroup.prototype.deactivateAnimationObject = function(index) {
	
	this.animation_objects_running[index] = false;
}