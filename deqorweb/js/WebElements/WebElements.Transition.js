// Transitions for Animations

var WebElements;

if(!WebElements) WebElements = {};

WebElements.Transition = function(from, to, form) {
	
	this.from = from;
	this.to = to;
	
	if(form != null && typeof form == "string")
	{
		this.form = form;
	}
	else
	{
		this.form = "linear";
	}
}

WebElements.Transition.prototype.getValueAt = function(progress) {
	
	switch(this.form)
	{
		case "linear":
			return ((this.to - this.from) * progress + this.from);
			break;
	
		case "square":
			return ((this.to - this.from) * Math.pow(progress, 2) + this.from);
			break;
			
		case "squareroot":
			return ((this.to - this.from) * Math.sqrt(progress) + this.from);
			break;
			
		case "sinusoidal":
			return ((this.to - this.from) * ((-Math.cos(progress * Math.PI) / 2) + 0.5) + this.from);
			break;
	}
	
	return false;
}