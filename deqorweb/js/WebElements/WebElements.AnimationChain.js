// List of AnimationGroups to be animated sequential

var WebElements;

if(!WebElements) WebElements = {};

WebElements.AnimationChain = function(animation_groups, callback) {
	
	this.animation_groups = new Array();
	this.animation_groups_running = new Array();
	
	if(animation_groups != null && typeof animation_groups.length != "undefined")
	{
		for(var i = 0; i < animation_groups.length; i++)
		{
			this.animation_groups.push(animation_groups[i]);
			this.animation_groups_running.push(false);
		}
	}
	else if(animation_groups != null)
	{
		this.animation_groups.push(animation_groups);
		this.animation_groups_running.push(false);
	}
	
	this.callback = null;
	this.setCallback(callback);
}

WebElements.AnimationChain.prototype.setCallback = function(callback) {
	
	if(callback != null && typeof callback == "function")
	{
		this.callback = callback;
		
		return true;
	}
	
	return false;
}

WebElements.AnimationChain.prototype.getAnimationCount = function() {
	
	return this.animation_groups.length;
}

WebElements.AnimationChain.prototype.addAnimationGroup = function(animation_group) {
	
	this.animation_groups.push(animation_group);
	this.animation_groups_running.push(false);
}

WebElements.AnimationChain.prototype.getAnimationGroup = function(index) {
	
	return this.animation_groups[index];
}

WebElements.AnimationChain.prototype.activateAnimationGroup = function(index) {
	
	this.animation_groups_running[index] = true;
}

WebElements.AnimationChain.prototype.deactivateAnimationGroup = function(index) {
	
	this.animation_groups_running[index] = false;
}