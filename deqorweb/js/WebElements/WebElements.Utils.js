var WebElements;

if(!WebElements) WebElements = {};

WebElements.Utils = new Object;

//---------------------------------------
// add additional functions to window.onload event
//---------------------------------------
WebElements.Utils.addLoadEvent = function(func) {
	
	var oldonload = window.onload;
	
	if(typeof window.onload != 'function')
	{
		window.onload = func;
	}
	else
	{
		if(oldonload)
		{
			window.onload = function() {
				oldonload();
				func();
			}
		}
		else
		{
			window.onload = func;
		}
	}
}

//---------------------------------------
// set a cookie
//---------------------------------------
WebElements.Utils.setCookie = function(name,value,expires,path,domain,secure) {

	document.cookie = name + "=" + escape(value) + 
		((expires) ? "; expires=" + expires.toGMTString() : "") +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") + ((secure) ? "; secure" : "");
}

//---------------------------------------
// get a cookie
//---------------------------------------
WebElements.Utils.getCookie = function(name) {

	var prefix = name + "=";
	var start = document.cookie.indexOf(prefix);

	if(start == -1)
	{
		return null;
	}

	var end = document.cookie.indexOf(";", start + prefix.length);
	
	if(end == -1) 
	{
		end = document.cookie.length;
	}

	var value = document.cookie.substring(start + prefix.length, end);
	
	return unescape(value);
}

//---------------------------------------
// delete a cookie
//---------------------------------------
WebElements.Utils.deleteCookie = function(name,path,domain) {

	if(this.getCookie(name))
	{
		document.cookie = name + "=" +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		"; expires=Thu, 01-Jan-70 00:00:01 GMT";
	}
}

//--------------------------------------------
// add an event listener to an html element
//--------------------------------------------
WebElements.Utils.addEventListener = function(elem, evnt, func, capt) {
	
	if(!capt) capt = false;
	//alert(func);

	if(elem.addEventListener)	// W3C model of adding events
	{
		elem.addEventListener(evnt, func, capt);
	}
	else if (elem.attachEvent)	// IE model of adding events
	{
		elem.attachEvent("on" + evnt, func)
	}
}

//--------------------------------------------
// remove an event listener from an html element
//--------------------------------------------
WebElements.Utils.removeEventListener = function(elem, evnt, func, capt) {

	if(!capt) capt = false;

	if(elem.removeEventListener)	// W3C model of removing events
	{
		elem.removeEventListener(evnt, func, capt);
	}
	else if (elem.detachEvent)		// IE model of removing events
	{
		elem.detachEvent("on" + evnt, func)
	}
}

//--------------------------------------------
// get the target of an event (the triggering element)
//--------------------------------------------
WebElements.Utils.getEventTarget = function(evnt) {

	var targ;
	
	if(evnt.target) targ = evnt.target;			// W3C model of event target
	else
	if(evnt.srcElement) targ = evnt.srcElement;		// IE model of event target
	
	// defeat bug in Safari (get parent of the text node)
	if(targ.nodeType == 3) targ = targ.parentNode;

	return targ;
}

//--------------------------------------------
// get the mouse position of a fired event
//--------------------------------------------
WebElements.Utils.getMousePosition = function(evnt) {

	var pos = new Object;
	pos.x = 0;
	pos.y = 0;
	
	if (evnt.pageX || evnt.pageY)
	{
		pos.x = evnt.pageX;
		pos.y = evnt.pageY;
	}
	else if (evnt.clientX || evnt.clientY)
	{
		pos.x = evnt.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		pos.y = evnt.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}
	
	return pos;
}

//--------------------------------------------
// get the mouse position of a fired event relative to the target
//--------------------------------------------
WebElements.Utils.getRelativeMousePosition = function(evnt) {

	var pos = this.getMousePosition(evnt);
	var pos_rel = this.absolutePosition(this.getEventTarget(evnt));
	var pos_scr = this.scrollPosition(element);
	
	pos.x = pos.x - pos_rel[0] + pos_scr[0];
	pos.y = pos.y - pos_rel[1] + pos_scr[1];
		
	return pos;
}

//--------------------------------------------
// get the mouse position of a fired event relative to another element
//--------------------------------------------
WebElements.Utils.getRelativeMousePosition = function(evnt, element) {

	var pos = this.getMousePosition(evnt);
	var pos_rel = this.absolutePosition(element);
	var pos_scr = this.scrollPosition(element);
	
	pos.x = pos.x - pos_rel[0] + pos_scr[0];
	pos.y = pos.y - pos_rel[1] + pos_scr[1];
	
	return pos;
}

//--------------------------------------------
// get the scroll positions of a document
//--------------------------------------------
WebElements.Utils.pageXOffset = function() {
	
   var w = window.pageXOffset ||
           document.body.scrollLeft ||
           document.documentElement.scrollLeft;
           
   return w ? w : 0;
}

WebElements.Utils.pageYOffset = function() {
	
   var h = window.pageYOffset ||
           document.body.scrollTop ||
           document.documentElement.scrollTop;
           
   return h ? h : 0;
}

//--------------------------------------------
// get the dimensions of a document
//--------------------------------------------
WebElements.Utils.pageWidth = function() {
	
	var w = document.body.offsetWidth ||
			document.body.clientWidth ||
			document.documentElement.clientWidth;
	
	return w ? w : 0;
}

WebElements.Utils.pageHeight = function() {
	
	var h = document.body.offsetHeight ||
			document.body.clientHeight ||
			document.documentElement.clientHeight;
	
	return h ? h : 0;
}

//--------------------------------------------
// get the dimensions of a window
//--------------------------------------------
WebElements.Utils.windowWidth = function() {
	
	var w = document.body.clientWidth ||
			document.body.clientWidth ||
			document.documentElement.clientWidth;
	
	return w ? w : 0;
}

WebElements.Utils.windowHeight = function() {
	
	var h = window.innerHeight ||
			screen.availHeight;		// not very nice, but in IE it's not possible to get the visible height of the window.
	
	return h ? h : 0;
}

//--------------------------------------------
// get the maximum dimensions of either the window or the document
//--------------------------------------------
WebElements.Utils.maxPageWidth = function() {
	
	var pw = WebElements.Utils.pageWidth();
	var ww = WebElements.Utils.windowWidth();
	
	return pw >= ww ? pw : ww;
}

WebElements.Utils.maxPageHeight = function() {
	
	var ph = WebElements.Utils.pageHeight();
	var wh = WebElements.Utils.windowHeight();
	
	return ph >= wh ? ph : wh;
}

//--------------------------------------------
// get the absolute position of an element
//--------------------------------------------
WebElements.Utils.absolutePosition = function(element) {
	
	var position = new Array();
	position[0] = 0;
	position[1] = 0;
	
	while(element)
	{
		position[0] += element.offsetLeft;
		position[1] += element.offsetTop;
		
		element = element.offsetParent;
	}
	
	return position;
}

//--------------------------------------------
// get the scroll position of an element
//--------------------------------------------
WebElements.Utils.scrollPosition = function(element) {
	
	var position = new Array();
	position[0] = 0;
	position[1] = 0;
	
	// get the scrolling amount from all the parents
	// intended for elements which are placed in scrollable divs
	while(element)
	{
		position[0] += element.scrollLeft;
		position[1] += element.scrollTop;
		
		element = element.offsetParent;
	}
	
	// as absolute position considers the page offset
	// subtract that offset here again to only return
	// the value of inner scrolling
	position[0] -= this.pageXOffset();
	position[1] -= this.pageYOffset();
	
	return position;
}

//--------------------------------------------
// get the relative position of an element
//--------------------------------------------
WebElements.Utils.relativePosition = function(element) {
	
	var position = new Array();
	position[0] = element.offsetLeft;
	position[1] = element.offsetTop;
	
	return position;
}

//--------------------------------------------
// determine if object is an array
//--------------------------------------------
WebElements.Utils.isArray = function(object) {

   if(object.constructor.toString().indexOf("Array") == -1)
      return false;
   else
      return true;
}

//--------------------------------------------
//determine if object is an int
//--------------------------------------------
WebElements.Utils.isInt = function(x) {
	
	var y = parseInt(x);

	if(isNaN(y)) return false;

	return (x == y && x.toString() == y.toString());
}

//--------------------------------------------
// get the code of a pressed key and return it
//--------------------------------------------
WebElements.Utils.checkKey = function(e) {
	
	var event;
	var keyCode;
	
	if(!e) 	event = window.event;	
	else	event = e;
	
	if(event.keyCode)	keyCode = event.keyCode;	// Internet Explorer
	else				keyCode = event.which;		// Firefox
	
	return keyCode;
}

//--------------------------------------------
// display the code of a pressed key 
//--------------------------------------------
WebElements.Utils.displayKey = function(e) {
	
	alert("Key pressed! Code: " + this.checkKey(e));
}

//--------------------------------------------
// disable the enter key
//--------------------------------------------
WebElements.Utils.disableEnterKey = function(e) {
	
     if(this.checkKey(e) == 13)	return false;
     else					return true;
}

//--------------------------------------------
// blur all links in a document
//--------------------------------------------
WebElements.Utils.blurAllLinks = function() {
	
	if(document.getElementsByTagName)
	{
		// get all anchors in the document
		var anchors = document.getElementsByTagName("a");
		
		// and add the blur() funtion to the onfocus event
		for(var i = 0; i < anchors.length; i++)
		{
			//WebElements.Utils.addEventListener(anchors[i], "focus", function() { if(navigator.appName == "Microsoft Internet Explorer")	{	alert("called this.blur()...");	} this.blur(); if(navigator.appName == "Microsoft Internet Explorer")	{	alert("end of this.blur()...");	} });
			anchors[i].setAttribute("onfocus", "this.blur();");
		}	
	}
}

//--------------------------------------------
// get all elements with the specified classname(s) 
// that have the specified tag name and are 
// children of the specified element
//--------------------------------------------
WebElements.Utils.getElementsByClassName = function(oElm, strTagName, oClassNames) {
	
	var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
	var arrReturnElements = new Array();
	var arrRegExpClassNames = new Array();
	
	if(typeof oClassNames == "object")
	{
		for(var i = 0; i < oClassNames.length; i++)
		{
			arrRegExpClassNames.push(new RegExp("(^|\\s)" + oClassNames[i].replace(/\-/g, "\\-") + "(\\s|$)"));
		}
	}
	else
	{
		arrRegExpClassNames.push(new RegExp("(^|\\s)" + oClassNames.replace(/\-/g, "\\-") + "(\\s|$)"));
	}
	
	var oElement;
	var bMatchesAll;
	
	for(var j = 0; j < arrElements.length; j++)
	{
		oElement = arrElements[j];
		bMatchesAll = true;
		
		for(var k = 0; k < arrRegExpClassNames.length; k++)
		{
			if(!arrRegExpClassNames[k].test(oElement.className))
			{
				bMatchesAll = false;
				break;
			}
		}
		if(bMatchesAll)
		{
			arrReturnElements.push(oElement);
		}
	}
	return (arrReturnElements)
}

//--------------------------------------------
// Array support for the push method in IE 5
//--------------------------------------------
if(typeof Array.prototype.push != "function")
{
	Array.prototype.push = ArrayPush;
	
	function ArrayPush(value) {
		
		this[this.length] = value;
	}
}

//--------------------------------------------
// Array.contains() implementation
//--------------------------------------------
Array.prototype.contains = function(element) {
	
	for(var i = 0; i < this.length; i++)
	{
		if(this[i] == element)
		{
			return true;
		}
	}
	
	return false;
}

//--------------------------------------------
// get the browser name
//--------------------------------------------
WebElements.Utils.getBrowserName = function() {
	
	var browser = navigator.userAgent;
	
	if(browser.indexOf(" Firefox/") >= 0) return "Firefox";
	if(browser.indexOf(" Safari/") >= 0) return "Safari";
	if(browser.indexOf(" MSIE ") >= 0) return "Microsoft Internet Explorer";
	
	return navigator.userAgent;
}

//--------------------------------------------
// get the browser version
//--------------------------------------------
WebElements.Utils.getBrowserVersion = function() {
	
	// to be redone...
	return navigator.appVersion;
}