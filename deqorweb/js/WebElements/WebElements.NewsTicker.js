var WebElements;

if(!WebElements) WebElements = {};

WebElements.NewsTicker = function(element, options) {
	
	this.element = element;			// container that holds the newsticker
	this.newsitems = null;			// the actual news items in the container
	this.slideprevious = null;		// element that forces previous news item to show up on click
	this.slidenext = null;			// element that forces next news item to show up on click
	this.slidewindow = null;		// container that frames the visible window in which the moving news items should be visible
	this.slidetoggle = null;		// element that toggles news items' movement on click
	this.newsitems_classname = "";	// class name of the news items in the container
	
	this.name = "newsticker";		// name of the newsticker instance for cookies etc.
	this.activenews = 0;			// the currently active news item
	this.nextnews = 0;				// the next news item
	this.previousnews = 0;			// the previous news item
	this.mode = "next";				// movement mode of the newsticker ("next" or "previous")
	this.running = false;			// determines if newsticker main loop is running
	
	this.use_cookies = true;
	this.cookie_path = "";
	
	this.timeout_id = 0;			// timeout id for the main loop of the newsticker (life-cycle)
	this.height = null;				// height of the slidewindow
	this.unit = "px";				// unit of the news items' position to be animated 
	this.fps = 60;					// frames per second of the animation
	this.duration = 1000;			// duration of a news item's movement
	this.lifecycle = 5000;			// duration of a news item's life-cycle (including display and movement)
	this.form = "linear";
	this.callback = null;
	this.animator = null;
	
	this.setOptions(options);
	
	this.init();
	
	this.attachBehaviors();
}

WebElements.NewsTicker.prototype.setOptions = function(options) {
	
	if(options) 
	{
		for(var optionname in options)
		{
			this[optionname] = options[optionname];
		}
		
		return true;
	}
	
	return false;
}

WebElements.NewsTicker.prototype.init = function() {
	
	if(this.newsitems == null)
	{
		this.newsitems = WebElements.Utils.getElementsByClassName(this.element, "*", this.newsitems_classname);
	}
	
	if(this.height == null)
	{
		this.height = this.slidewindow.offsetHeight;
	}
	
	this.slidewindow.style.height = this.height + this.unit;
	this.slidewindow.style.overflow = "hidden";
	this.slidewindow.style.position = "relative";
	
	for(var i = 0; i < this.newsitems.length; i++)
	{
		this.newsitems[i].style.position = "absolute";
		
		if(i != this.activenews)
		{
			this.newsitems[i].style.top = this.height + this.unit;
		}
	}
	
	this.updateNewsItems("current");
}

WebElements.NewsTicker.prototype.attachBehaviors = function() {
	
	var self = this;
	
	if(this.slideprevious) WebElements.Utils.addEventListener(this.slideprevious, "click", function(e) { self.previousNewsItem(); });
	if(this.slidenext) WebElements.Utils.addEventListener(this.slidenext, "click", function(e) { self.nextNewsItem(); });
	if(this.slidetoggle) WebElements.Utils.addEventListener(this.slidetoggle, "click", function(e) { self.toggleMainLoop(); });
}

WebElements.NewsTicker.prototype.updateNewsItems = function(mode) {
	
	switch(mode)
	{
		case "current":
					// do nothing - only let nextnews and previousnews be calculated
					break;
					
		case "next":
					this.activenews++;
					
					if(this.activenews == this.newsitems.length)
					{
						this.activenews = 0;
					}
					break;
					
		case "previous":
					this.activenews--;
					
					if(this.activenews < 0)
					{
						this.activenews = this.newsitems.length - 1;
					}
					break;
	}
	
	this.nextnews = this.activenews + 1;
	this.previousnews = this.activenews - 1;
	
	if(this.nextnews == this.newsitems.length) this.nextnews = 0;
	if(this.previousnews < 0) this.previousnews = this.newsitems.length - 1;
}

WebElements.NewsTicker.prototype.nextNewsItem = function() {
	
	if(!this.animator || !this.animator.running)
	{
		var self = this;
		
		// create animator object with initial animation chain (empty),
		// defined frames per second and a callback function
		this.animator = new WebElements.Animator(new WebElements.AnimationChain(), this.fps, function() { self.updateNewsItems("next"); });
		
		// create animations to be executed
		var animation_1 = new WebElements.AnimationObject(this.newsitems[this.activenews], "top", 0, -this.height, this.unit, this.duration, null, this.form);
		var animation_2 = new WebElements.AnimationObject(this.newsitems[this.nextnews], "top", this.height, 0, this.unit, this.duration, null, this.form);
		
		// create groups for animation objects and put them into
		// the animation chain
		this.animator.animation_chain.addAnimationGroup(new WebElements.AnimationGroup(new Array(animation_1, animation_2)));
		
		this.animator.start();
		
		return true;
	}
	
	return false;
}

WebElements.NewsTicker.prototype.previousNewsItem = function() {
	
	if(!this.animator || !this.animator.running)
	{
		var self = this;
		
		// create animator object with initial animation chain (empty),
		// defined frames per second and a callback function
		this.animator = new WebElements.Animator(new WebElements.AnimationChain(), this.fps, function() { self.updateNewsItems("previous"); });
		
		// create animations to be executed
		var animation_1 = new WebElements.AnimationObject(this.newsitems[this.activenews], "top", 0, this.height, this.unit, this.duration, null, this.form);
		var animation_2 = new WebElements.AnimationObject(this.newsitems[this.previousnews], "top", -this.height, 0, this.unit, this.duration, null, this.form);
		
		// create groups for animation objects and put them into
		// the animation chain
		this.animator.animation_chain.addAnimationGroup(new WebElements.AnimationGroup(new Array(animation_1, animation_2)));
		
		this.animator.start();
		
		return true;
	}
	
	return false;
}

WebElements.NewsTicker.prototype.startMainLoop = function() {
	
	this.running = true;
	
	var self = this;
	
	this.timeout_id = setTimeout(function() { self.mainLoop(); }, this.lifecycle);
}

WebElements.NewsTicker.prototype.stopMainLoop = function() {
	
	clearTimeout(this.timeout_id);
	
	this.running = false;
}

WebElements.NewsTicker.prototype.toggleMainLoop = function() {
	
	if(this.running) 	this.stopMainLoop();
	else 				this.startMainLoop();
}

WebElements.NewsTicker.prototype.mainLoop = function() {
	
	var self = this;
	
	if(this.mode == "next") 	this.nextNewsItem();
	if(this.mode == "previous") this.previousNewsItem();
	
	this.timeout_id = setTimeout(function() { self.mainLoop(); }, this.lifecycle);
}
