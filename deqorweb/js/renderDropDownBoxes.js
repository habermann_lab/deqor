dropboxcount = 0;

function renderDropDownBoxes() {

	var boxes = WebElements.Utils.getElementsByClassName(document.body, "div", "dropdown");

	for(var i = 0; i < boxes.length; i++)
	{
		var header = WebElements.Utils.getElementsByClassName(boxes[i], "*", "dropdownheader")[0];

		if(!header)
		{
			header = boxes[i].firstChild;

			while(header && header.nodeType != 1)
			{
				header = header.nextSibling;
			}
		}

		var body = WebElements.Utils.getElementsByClassName(boxes[i], "*", "dropdownbody")[0];

		if(!body)
		{
			body = header.nextSibling;

			while(body && body.nodeType != 1)
			{
				body = body.nextSibling;
			}
		}

		if(header != null && body != null)
		{
			var dropdownbox = new WebElements.DropDownBox(boxes[i], {name:'ddbox' + i, state:0, activator:header, reactor:body, duration:300, use_cookies:false});
		}
		
		dropboxcount++;
	}
}

function addDropDownBox(box) {
	
	var box = box;
	var header = WebElements.Utils.getElementsByClassName(box, "*", "dropdownheader")[0];
	if(!header)
	{
		header = box.firstChild;

		while(header && header.nodeType != 1)
		{
			header = header.nextSibling;
		}
	}
	var body = WebElements.Utils.getElementsByClassName(box, "*", "dropdownbody")[0];
	if(!body)
	{
		body = header.nextSibling;

		while(body && body.nodeType != 1)
		{
			body = body.nextSibling;
		}
	}

	if(header != null && body != null)
	{
		var dropdownbox = new WebElements.DropDownBox(box, {name:'ddbox' + dropboxcount, state:0, activator:header, reactor:body, duration:300, use_cookies:false});
	}
	
	dropboxcount++;
}

WebElements.Utils.addLoadEvent(renderDropDownBoxes);