Dialog = function(parent, options) {
	
	this.element = null;
	this.message = null;
	this.parent = parent;
	
	this.size = null;
	this.position = null;
	this.message_text = "";
	
	this.setOptions(options);
	
	this.init();
}

Dialog.prototype.setOptions = function(options) {
	
	if(options) 
	{
		for(var optionname in options)
		{
			this[optionname] = options[optionname];
		}
		
		return true;
	}
	
	return false;
}

Dialog.prototype.init = function() {
	
	this.element = document.createElement("div");
	this.element.className = "dialog";
	
	if(this.size != null)
	{
		this.element.style.width = this.size[0] + "px";
		this.element.style.height = this.size[1] + "px";
	}
	
	if(this.position != null)
	{
		this.element.style.position = "absolute";
		this.element.style.left = this.position[0] + "px";
		this.element.style.top = this.position[1] + "px";
	}
	
	if(this.message_text != "")
	{
		this.message = document.createTextNode(this.message_text);
		
		this.element.appendChild(this.message);
	}
}

Dialog.prototype.show = function() {
	
	if(this.parent != null)
	{
		this.parent.appendChild(this.element);
	}
	else
	{
		document.getElementsByTagName("body")[0].appendChild(this.element);
	}
}

Dialog.prototype.close = function() {
	
	if(this.parent != null)
	{
		this.parent.removeChild(this.element);
	}
	else
	{
		document.getElementsByTagName("body")[0].removeChild(this.element);
	}
}

Dialog.prototype.setMessageText = function(text) {
	
	this.message.nodeValue = text;
}