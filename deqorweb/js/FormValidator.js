
// FormValidator Class

FormValidator = function(form_name, options) {
	
	this.form = null;
	this.inputs = null;
	
	this.form_name = form_name;
	
	this.error = false;
	this.error_required = "Please type in a value.";
	this.error_type = "Please type in a###TYPE### value.";
	this.error_range = "Please type in a value between ###LOWRANGE### and ###HIGHRANGE###.";
	
	this.setOptions(options);
	
	this.init();
}

FormValidator.prototype.setOptions = function(options) {
	
	if(options) 
	{
		for(var optionname in options)
		{
			this[optionname] = options[optionname];
		}
		
		return true;
	}
	
	return false;
}

FormValidator.prototype.init = function() {
	
	this.form = document.forms[this.form_name];
	
	var self = this;
	
	this.form.onsubmit = function() { return self.checkForm(); };
	
	this.inputs = new Array();
}

FormValidator.prototype.setOnSubmit = function(func) {
	
	var self = this;
	
	this.form.onsubmit = function() { 
							
							if(self.checkForm()) 
							{
								return func();
							}
							else return false;
						};
}

FormValidator.prototype.addInput = function(name, eval, event) {
	
	if(event == null) event = "change";
	
	this.inputs.push(new FormInput(this.form.elements[name], this.inputs.length, this, {event:event, eval:eval}));
}

FormValidator.prototype.checkForm = function() {
	
	this.error = false;
	
	for(var i = 0; i < this.inputs.length; i++)
	{
		if(!this.inputs[i].check())
		{
			this.error = true;
		}
	}
	
	return (!this.error);
}

FormValidator.prototype.formToJson = function() {
	
	var json = new Object();
	
	for(var i = 0; i < this.form.elements.length; i++)
	{
		json[this.form.elements[i].name] = this.form.elements[i].value;
	}
	
	return json;
}


// FormInput Class

FormInput = function(element, index, parent, options) {
	
	this.element = element;
	this.index = index;
	this.parent = parent;
	this.dialog = null;
	
	this.event = "change";
	this.eval = null;
	
	this.error = false;
	this.error_classname = "error";
	this.error_message = "";
	
	this.setOptions(options);
	
	this.init();
}

FormInput.prototype.setOptions = function(options) {
	
	if(options) 
	{
		for(var optionname in options)
		{
			this[optionname] = options[optionname];
		}
		
		return true;
	}
	
	return false;
}

FormInput.prototype.init = function() {
	
	var self = this;
	
	WebElements.Utils.addEventListener(this.element, this.event, function(){ self.check(); });
}

FormInput.prototype.check = function() {
	
	this.error = false;
	this.error_message = "";
	
	// check if required
	if(this.eval["required"] != null && this.eval["required"] == true)
	{
		if(this.element.value == "")
		{
			this.error = true;
			this.error_message += this.parent.error_required;
		}
	}
	// check all other restrictions if a value is set
	if(this.element.value != "")
	{
		// check type "int"
		if(this.eval["type"] != null && this.eval["type"] == "int")
		{
			if(!WebElements.Utils.isInt(this.element.value))
			{
				this.error = true;
				this.error_message += this.parent.error_type.replace("###TYPE###", "n int");
			}
		}
		// check range
		if(this.eval["range"] != null && this.eval["range"] != "")
		{
			var range = this.eval["range"].split(",");
			
			if(!isNaN(range[0])) range[0] = Number(range[0]);
			if(!isNaN(range[1])) range[1] = Number(range[1]);
			
			if(this.element.value < range[0] || this.element.value > range[1])
			{
				this.error = true;
				this.error_message += this.parent.error_range.replace("###LOWRANGE###", range[0]).replace("###HIGHRANGE###", range[1]);
			}
		}
	}
	
	if(this.error)
	{
		if(this.element.className.indexOf(" " + this.error_classname) == -1)
		{
			this.element.className += " " + this.error_classname;
		}
		
		if(this.dialog == null)
		{
			this.dialog = new Dialog(this.element.parentNode, {message_text:this.error_message});
			this.dialog.show();
		}
		else
		{
			this.dialog.setMessageText(this.error_message);
		}
		
		return false;
	}
	else
	{
		this.element.className = this.element.className.replace(this.error_classname, "");
		
		if(this.dialog != null)
		{
			this.dialog.close();
			delete this.dialog;
		}
		
		return true;
	}
}

