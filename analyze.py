import os
from pickle import dump
import re
nonNuclRegex = re.compile('[^AGTCN]')
outputDir = '/export/deqor_v3/output'
minSeqLength = 21
bowtiedbloc = '/export/rnadb'

def analyze(id, params):
    discard = os.system(('mkdir %s' % os.path.join(outputDir, id)))
    try: sequence = nonNuclRegex.subn('', params['FASTA'].upper().replace("U","T"))[0]
    except KeyError: return

    seqFile = os.path.join(outputDir, "%s.fasta" % id)
    file = open(seqFile, "w")
    file.write(">%s\n" % id)
    file.write("%s\n" % sequence)
    file.close()

    bowtiedb = os.path.join(bowtiedbloc, params['GENOME'])

    outputfolder = os.path.join(outputDir, id)

    discard = os.system("python pipeline.py %s %s %s" % (seqFile, bowtiedb, outputfolder))

    return 

