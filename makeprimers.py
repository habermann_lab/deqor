from sys import argv
import primerdesign

forprimers = [line.strip().split(",") for line in open(argv[1]).readlines()]
opfile = open(argv[2],"w")
for entity in forprimers: 
    riddle_id, sequence, hu_id = entity[0], entity[4], entity[20]
    id = "%s___%s" % (riddle_id, hu_id)
    primerdesign.writePrimerInputFile(sequence, id)
    primerline = primerdesign.designExtractPrimers(id)
    opfile.write("%s\n" % primerline)
opfile.close()
