
def constructSeq(dqOp):
	"""
	input : deqor output of the form [(index, seq, csQual, Qual),(...),...]
	output : sequence from deqor output, we do this because sometimes it is the common region analyzed
	"""
	if not len(dqOp): return ""
	return "".join([item[1][0] for item in dqOp]) + dqOp[-1][1][1:] # dqOp of the form [(index, seq, csQual, Qual),(...),...]

def getPrimaryData(id, db, mapFlag, isGene):

	"""
	input = id to look for, which database : human|mouse|rat|ensembl, id is gene or trns
	output = tuple of tuples, ((esirna, location, primerleft, primerright, good),(...),..)
	"""
	givenId, givenDb = id, db
	if mapFlag:
		discard = curs.execute("select ensembl from ensncbimap where human='%s'" % id)
		records = curs.fetchall()
		if len(records): id, db = records[0][0], 'ensembl'

   # transcript is a direct search, gene in case of ncbi - check in synonyms as well
	if not isGene: 
		discard = curs.execute("select esirna, loc, primerleft, primerright, good from %s where transcripts like '%%%s%%'" % (db, id))
		if mapFlag:
			records = curs.fetchall()
			if not len(records): db, id = givenDb, givenId
			discard = curs.execute("select esirna, loc, primerleft, primerright, good from %s where transcripts like '%%%s%%'" % (db, id))
	else: 
		discard = curs.execute("select esirna, loc, primerleft, primerright, good from %s where id='%s'" % (db, id))
		records = curs.fetchall()
		if (not len(records)) and (db != 'ensembl'):
			records = curs.fetchall()
			discard = curs.execute("select esirna, loc, primerleft, primerright, good from %s where synonyms like '%%%s%%'" % (db,id))

	if givenId != id: id = "%s__%s" % (givenId, id)
	return (id, curs.fetchall())


def locateEsiInDeqor(esi, deqorOp):

	"""
	input = primary esirna, deqor output relating to this esirna
	output = position of esirna when found, else -1
	"""

	return constructSeq(deqorOp).find(esi)

	
