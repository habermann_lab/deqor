overlap = 400
mismatch_threshold = 10

def Decimal2Binary(dec_num):
	if dec_num == 0: return ''
	head, tail = divmod(dec_num, 2)
	return Decimal2Binary(head) + str(tail)

def lcsString(sequences, mismatch_threshold=10):

	suffixes = []
	count = 1
	
	for seq in sequences:
		length = seq.__len__()
		for index in range(0, length):
			suffixes.append(seq[index:length] + "%s" % count)
		count += 1
	
	suffixes.sort()
	
	maxlen = 0
	maxstr = ""
	
	parents_set = range(1, sequences.__len__() + 1)
	
	for num in range(0, suffixes.__len__() - (sequences.__len__() - 1)):
		curr = suffixes[num : num + sequences.__len__()]
		parents = [int(suffix[-1]) for suffix in curr]
		parents.sort()
		if parents != parents_set: continue

		curr = [suffix[:-1] for suffix in curr]
		length = 0
		mismatch = 0
		for index in range(curr[0].__len__()):
			curr_char = curr[0][index]
			match = True
			
			for suffix in curr[1:]:
				try: 
					if curr_char != suffix[index]:
						mismatch += 1
						if mismatch > mismatch_threshold: 
							match = False
							break
				except IndexError: break
			if match: 
				length = length + 1
			else: break
		if length > maxlen:
			maxlen = length
			maxstr = curr[0][0:length]
			maxall = curr
	return (maxlen, maxstr)


def fastaToSeqHash(fastaFile):

	parser = Fasta.RecordParser()
	iter = Fasta.Iterator(open(fastaFile), parser)

	curr = iter.next()
	seqHash = {}

	while curr is not None:
		seqHash[curr.title] = curr.sequence
		curr = iter.next()
	return seqHash




def overlapNoCulprit(seqhash):

	common_sets = []
	firstRun = True

	while firstRun or (len(common) and (len(culprits) > 1)):

		if firstRun:
			print "Analyzing : ", " ".join(seqhash.keys())
		else:
			print "Found a region for : ", " ".join(used)
			print "Analyzing : ", " ".join(culprits)

		seqids = seqhash.keys()
		totalnum = len(seqids)
		binhash = {}
		maxlen = 0
		
		for num in range(pow(2, totalnum) - 1, 0, -1):
			repr = Decimal2Binary(num)
			num_of_ones = repr.count("1")
			if num_of_ones > 1:
				if binhash.has_key(num_of_ones): binhash[num_of_ones].append(repr)
				else: binhash[num_of_ones] = [repr]
		
		numof1s = binhash.keys()
		numof1s.sort()
		numof1s.reverse()
		
		foranalysis = []
		
		for key in numof1s:
			for set in binhash[key]:
				ids = []
				for index in range(len(set)):
					if set[-1 - index] == "1":
						ids.append(seqids[-1 - index])
				foranalysis.append(ids)
		
		for set in foranalysis:
			sequences = [seqhash[id] for id in set]
			lcs_op = lcsString(sequences)
			if lcs_op[0] > overlap: 
				maxlen = lcs_op[0]
				usedset = set
				break
	
		common, culprits, used = "", [], []
		
		if maxlen:
			common = lcs_op[1]
			for id in seqids:
				if id not in usedset: culprits.append(id)
				else: used.append(id)

		if len(common): common_sets.append((common, used))
		newseqhash = {}
		for id in culprits: newseqhash[id] = seqhash[id]
		seqhash = newseqhash
		firstRun = False
	
	return (common_sets, culprits)
			
	
	


	



	
	
	
	
	
	
