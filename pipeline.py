# The esiRNA design pipeline
# hopefully this is the end of the travesty of BLAST parsing, and the end of the shadow of 2004

# Please ensure that you have biopython and bowtie installed, and the path to the bowtie binary is set in the shell's PATH variable

# Python modules
from sys import argv
from os import path, mkdir, system
from pickle import dump
from Bio import SeqIO

# Home-brewed modules
from offtargets import makesirnafile, scoreofftargets, runbowtie
from quality import Quality

seq_fasta_file = argv[1]
bowtie_db = argv[2]
op_folder = argv[3]

seq_iterator = SeqIO.parse(open(seq_fasta_file), "fasta")
for seq_object in seq_iterator:
        seqid, sequence = seq_object.name, str(seq_object.seq)

        print "Processing : ", seqid
        sirna_fasta = path.join(op_folder, "sirnas.txt")
        makesirnafile(sequence, sirna_fasta)

        print "running bowtie"
        bowtie_opfile = path.join(op_folder, "bowtieop.txt")
        runbowtie(sirna_fasta, bowtie_db, bowtie_opfile)

        print "scoring offtargets"
        offtargets_hash = scoreofftargets(bowtie_opfile, sequence)
        deqor_op = []

        print "calculating qualities"
        sirna_iterator = SeqIO.parse(open(sirna_fasta), "fasta")
        sihash = {}
        othash = {}
        maxqual = 0
        for sirna_object in sirna_iterator:
                sirna_id, sirna_seq = sirna_object.name, str(sirna_object.seq)
                sirna_quality_object = Quality(sirna_seq)
                quality_penalties = sirna_quality_object.totalPenalties
                if quality_penalties > maxqual: maxqual = quality_penalties

                if offtargets_hash.has_key(sirna_id): 
                        offtargets_penalty = reduce(lambda x, y: x + y, [offtargets_hash[sirna_id][trnsid] for trnsid in offtargets_hash[sirna_id].keys()])
                        offtargets = []
			for item in offtargets_hash[sirna_id].keys():
				if item.find("|") != -1: offtargets.append(item.split("|")[3].split(".")[0])
				else: offtargets.append(item.split()[0])
                else: offtargets_penalty,offtargets = 0, []

                
                sihash[sirna_id] = {'efficiency':str(quality_penalties),'sequence':sirna_seq}
                otidhash = {}
                for otindex in range(len(offtargets)): otidhash[str(otindex)] = offtargets[otindex]
                othash[sirna_id] = {'csscore':str(quality_penalties + offtargets_penalty),'ids':otidhash}

		deqor_op.append((sirna_id, sirna_seq, quality_penalties + offtargets_penalty, quality_penalties))
                del(sirna_quality_object)




        qualhash = {'windows':sihash,'maximum':str(maxqual),'sequence':sequence}
        qualfile = open(path.join(op_folder, "qual.dump"),"w")
        dump(qualhash, qualfile)
        qualfile.close()

        offfile = open(path.join(op_folder, "offtargets.dump"), "w")
        dump(othash, offfile)
        offfile.close()

        discard = system("touch %s" % path.join(op_folder, "qualdone.flag"))
        discard = system("touch %s" % path.join(op_folder, "offtargetsdone.flag"))

